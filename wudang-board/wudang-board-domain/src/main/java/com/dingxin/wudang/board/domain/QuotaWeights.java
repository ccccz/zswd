package com.dingxin.wudang.board.domain;

public class QuotaWeights {
    private String quotaId;
    private String weights;
    private String weightsTime;
    private String menuItem;

    public String getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(String menuItem) {
        this.menuItem = menuItem;
    }

    public String getQuotaId() {
        return quotaId;
    }

    public void setQuotaId(String quotaId) {
        this.quotaId = quotaId;
    }

    public String getWeights() {
        return weights;
    }

    public void setWeights(String weights) {
        this.weights = weights;
    }

    public String getWeightsTime() {
        return weightsTime;
    }

    public void setWeightsTime(String weightsTime) {
        this.weightsTime = weightsTime;
    }

    public QuotaWeights(String quotaId, String weights, String weightsTime, String menuItem) {
        super();
        this.quotaId = quotaId;
        this.weights = weights;
        this.weightsTime = weightsTime;
        this.menuItem = menuItem;
    }

    public QuotaWeights() {
        super();
    }

    @Override
    public String toString() {
        return "QuotaWeights{" +
                "quotaId='" + quotaId + '\'' +
                ", weights='" + weights + '\'' +
                ", weightsTime='" + weightsTime + '\'' +
                ", menuItem='" + menuItem + '\'' +
                '}';
    }
}
