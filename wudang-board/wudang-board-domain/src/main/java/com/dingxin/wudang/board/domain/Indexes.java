package com.dingxin.wudang.board.domain;

/**
 * 指标录入实体类
 */
public class Indexes {
    /**
     * 数据id
     */
    private String id;
    /**
     * 单位编码
     */
    private String orgCode;
    /**
     * 上级单位编码
     */
    private String prevOrgCode;
    /**
     * 指标id
     */
    private String quotaId;
    /**
     * 完成值
     */
    private String completeValue;
    /**
     * 目标值
     */
    private String targetValue;
    /**
     * 指标时间
     */
    private String quotaTime;
    /**
     * 指标名称
     */
    private String quotaName;

    /**
     * 录入人员id
     */
    private String writePersonId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getPrevOrgCode() {
        return prevOrgCode;
    }

    public void setPrevOrgCode(String prevOrgCode) {
        this.prevOrgCode = prevOrgCode;
    }

    public String getQuotaId() {
        return quotaId;
    }

    public void setQuotaId(String quotaId) {
        this.quotaId = quotaId;
    }

    public String getCompleteValue() {
        return completeValue;
    }

    public void setCompleteValue(String completeValue) {
        this.completeValue = completeValue;
    }

    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    public String getQuotaTime() {
        return quotaTime;
    }

    public void setQuotaTime(String quotaTime) {
        this.quotaTime = quotaTime;
    }

    public String getQuotaName() {
        return quotaName;
    }

    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getWritePersonId() {
        return writePersonId;
    }

    public void setWritePersonId(String writePersonId) {
        this.writePersonId = writePersonId;
    }
}
