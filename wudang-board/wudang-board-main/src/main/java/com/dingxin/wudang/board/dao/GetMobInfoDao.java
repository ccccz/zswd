package com.dingxin.wudang.board.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 移动指标Dao
 *
 * @author zhicheng.zhang
 */

public interface GetMobInfoDao {


    /**
     * 根据年月查询移动端需要展示的指标
     *
     * @return
     */
    List<Map<String, Object>> showMobInfo(@Param("queryMonth") String queryMonth, @Param("porm") String porm);

    /**
     * 获取一年中的完成值和目标值
     *
     * @param indexId
     * @return
     */
    List<Map<String, Object>> crValueOfYear(@Param("id") String indexId);

    /**
     * 查询当前年中各个月
     *
     * @return
     */
    List<String> queryDateOfYear();

    /**
     * 查询某局下供电所信息
     *
     * @param orgCode
     * @return
     */
    List<Map<String, Object>> queryGDS(@Param("orgCode") String orgCode);

    /**
     * 查询供电所及台区数据
     *
     * @param queryMonth
     * @param orgCode
     * @param itemId
     * @return
     */
    List<Map<String, Object>> queryGDSData(@Param("queryMonth") String queryMonth, @Param("orgCode") String orgCode, @Param("indexId") String itemId);

    /**
     * 查询台区编码
     *
     * @return
     */
    List<Map<String, Object>> queryTQData(@Param("map") Map<String, Object> map);

    String isInportent(String indexID);
}
