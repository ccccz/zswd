package com.dingxin.wudang.board.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Common Method Dao
 * 公共方法dao
 *
 * @author Cccccz
 * @date 2018-11-26
 */
public interface CommonDao {

    /**
     * 获取计算同比环比的数据
     *
     * @param quotaId
     * @param boardTime
     * @param orgCode
     * @return
     */
    List<String> getRingRatioData(@Param("quotaId") String quotaId, @Param("boardTime") String boardTime, @Param("orgCode") String orgCode);

    /**
     * 获取当前看板指标id
     *
     * @param porm 看板编号
     * @return
     */
    List<String> getQuotaId(@Param("porm") int porm);

    /**
     * 获取对应看板数据时间列表
     *
     * @param boardNum
     * @return
     */
    List<String> getQueryMonth(@Param("boardNum") String boardNum);

    /**
     * 根据时间和看板编码查询看板数据
     *
     * @param map
     * @return
     */
    List<Map<String, Object>> getQuotaByMonth(@Param("map") Map map);
}
