package com.dingxin.wudang.board.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Station ,Line ,Transformer, User Dao
 * 站线变户Dao层
 *
 * @author Ccccz
 * @date 2018-11-28
 */
public interface CompanyDao {

    /**
     * Get Station Data
     * 获取变电站数据
     *
     * @return
     */
    List<Map<String, Object>> getData(@Param("type") String type);


    List<Map<String, Object>> getSubData(@Param("orgCode") String orgCode, @Param("typeid") String typeid, @Param("type") String string);


}
