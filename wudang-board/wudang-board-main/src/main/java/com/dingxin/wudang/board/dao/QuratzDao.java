package com.dingxin.wudang.board.dao;

import org.apache.ibatis.annotations.Param;

/**
 * @author Administrator
 */
public interface QuratzDao {

    /**
     * 获取指标id
     *
     * @param sort 指标排名序号
     * @return
     */
    String getQuotaId(@Param("sort") int sort);

    /**
     * 获取最新数据时间
     *
     * @param quotaId
     * @return
     */
    String getCommitDate(@Param("quotaId") String quotaId);
}
