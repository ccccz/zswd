package com.dingxin.wudang.board.controller;

import com.dingxin.wudang.board.common.CommonMethod;
import com.dingxin.wudang.board.dao.GetWebConnDao;
import com.dingxin.wudang.board.domain.Indexes;
import com.dingxin.wudang.board.domain.QuotaWeights;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.board.utils.UUIDUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Web端接口
 *
 * @author zhicheng.zhang
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class WebController {

    @Autowired
    GetWebConnDao getCon;

    @Autowired
    CommonMethod comm;

    private static final boolean TRUE = true;
    private static final boolean FALSE = false;
    private static final String QUERYMONTH = "queryMonth";
    private static final String COMPLETEVALUE = "completeValue";
    private static final String TARGETVALUE = "targetValue";
    private static final String WDCODE = "060101";
    private static final String INDEXMANAGEMENT = "indexManagement";
    private static final String POSITIONLEVEL = "positionLevel";
    private static final String CHILDREN = "children";
    private static final String PROVINCERANKED = "provinceRanked";
    private static final String ORGCODE = "orgCode";
    private static final String ISIMPORTENT = "isImportent";
    private static final String COUNTYRANKED = "countyRanked";
    private static final String STATIONRANKED = "stationRanked";
    private static final String YEARBASIS = "yearBasis";
    private static final String RELATIVERATIO = "relativeRatio";
    private static final String CURRENTDATA = "currentData";
    private static final String ACCOMPLISHCOMP = "accomplishComp";
    private static final String PARTICULARS = "particulars";


    @RequestMapping(value = "/indicator/insert", method = RequestMethod.POST, produces = {"application/json;charset=UTF-8"})
    public String insert(@RequestBody Map<String, String> map) throws JsonProcessingException {
        String type = "quota";
        String id = UUIDUtil.getUUID();
        String station = map.get("station");
        Map<String, String> indexMenu = getCon.findIndexNameByCheckMenu(map.get("menuItem"));
        String prevOrgCode = station.substring(0, station.length() - 2);

        Indexes indexes = new Indexes();
        indexes.setId(id);
        indexes.setOrgCode(station);
        indexes.setPrevOrgCode(prevOrgCode);
        indexes.setQuotaId(indexMenu.get("id"));
        indexes.setQuotaName(indexMenu.get("name"));
        indexes.setQuotaTime(map.get(QUERYMONTH).replace("-", ""));
        indexes.setCompleteValue(map.get(COMPLETEVALUE));
        indexes.setTargetValue(map.get(TARGETVALUE));
        indexes.setWritePersonId(map.get("userid"));

        if (getCon.duplicateData(indexes.getQuotaId(), indexes.getQuotaTime(), indexes.getOrgCode(), type) > 0) {
            getCon.updateQuotaData(indexes);
        } else {
            getCon.addIndexInfo(indexes);
        }
        return JsonUtil.objToJson(Result.success("", "成功"));
    }

    /**
     * 获取供电单位信息
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/user/company", method = RequestMethod.GET)
    public String getUserCompany() throws JsonProcessingException {
        return JsonUtil.objToJson(Result.success(getCon.findOrgNameByOrgCode(WDCODE), "查询成功"));
    }

    /**
     * 存储权重信息
     *
     * @param map 权重信息集合
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/indicator/saveWeights", method = RequestMethod.POST)
    public String saveWeights(@RequestBody Map<String, String> map) throws JsonProcessingException {
        Set<String> keySet = map.keySet();
        BigDecimal sum = BigDecimal.valueOf(0);
        for (String s : keySet) {
            if (!QUERYMONTH.equals(s)) {
                sum = sum.add(BigDecimal.valueOf(Double.parseDouble(map.get(s))));
            }
        }
        if (sum.doubleValue() == 1) {
            for (String s : keySet) {
                if (!QUERYMONTH.equals(s)) {
                    Map<String, String> menuList = getCon.findIndexNameByCheckMenu(s);
                    QuotaWeights quotaWeights = new QuotaWeights(menuList.get("id"), map.get(s), map.get(QUERYMONTH).replace("-", ""), s);
                    if (getCon.duplicateData(quotaWeights.getQuotaId(), quotaWeights.getWeightsTime().replace("-", ""), "", "weights") > 0) {
                        getCon.updateWeightsData(quotaWeights);
                    } else {
                        getCon.addIndexWeights(quotaWeights);
                    }
                }
            }
            return JsonUtil.objToJson(Result.success("", "成功"));
        }
        return JsonUtil.objToJson(Result.fail(608, "添加失败"));
    }

    /**
     * WEB端首页页面展示
     *
     * @param queryMonth 查询日期
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/indicator/all", method = RequestMethod.GET)
    public String getWebShowInfo(@RequestParam String queryMonth) throws JsonProcessingException {
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, String> indexManagement;
        int length = 4;
        if (comm.canFormat(queryMonth) == TRUE) {
            if (queryMonth.length() > length) {
                String qMonth = queryMonth.replace("-", "");
                Map<String, Object> datalist = allInfoMap(qMonth);
                indexManagement = makeIndexManagement(qMonth);
                datalist.put(INDEXMANAGEMENT, indexManagement);
                data.add(datalist);
            } else {
                List<String> monthList = getCon.findMonthOfYear(queryMonth, "", "");
                for (String month : monthList) {
                    Map<String, Object> datalist = allInfoMap(month);
                    indexManagement = makeIndexManagement(month);
                    datalist.put(INDEXMANAGEMENT, indexManagement);
                    data.add(datalist);
                }
            }
        } else {
            List<String> monthList = getCon.findAllMonth("");
            for (String s : monthList) {
                Map<String, Object> datalist = new HashMap<>(16);
                datalist.putAll(allInfoMap(s));
                indexManagement = makeIndexManagement(s);
                datalist.put(INDEXMANAGEMENT, indexManagement);
                data.add(datalist);

            }
        }
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 获取具体的指标详情
     *
     * @param queryMonth 查询日期
     * @param id         指标id
     * @return
     */
    @RequestMapping(value = "/indicator/detail")
    public String getSpecificInfo(String queryMonth, String id) throws JsonProcessingException {
        List<Map<String, Object>> data = new ArrayList();
        Map<String, Object> datas;
        int length = 4;
        if (comm.canFormat(queryMonth) == TRUE) {
            if (queryMonth.length() > length) {
                String qMonth = queryMonth.replace("-", "");
                datas = allDetail(qMonth, id);

                data.add(datas);

            } else {
                List<String> monthList = getCon.findMonthOfYear(queryMonth, "", "");
                for (String month : monthList) {
                    datas = allDetail(month, id);
                    data.add(datas);
                }
            }
        } else {
            List<String> monthList = getCon.findAllMonth("");
            for (String s : monthList) {
                if (getCon.findIndexInfoByMonthAndId(s, id).isEmpty() == FALSE) {
                    datas = allDetail(s, id);
                    data.add(datas);
                    break;
                }
            }
        }
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 10KV信息
     *
     * @param queryMonth 查询日期
     * @param positionId 单位编码
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/indicator/lineLossDetail", method = RequestMethod.GET)
    public String getLineLoss(String queryMonth, String positionId) throws JsonProcessingException {
        String id = getCon.queryIndexIdByIsImportent(1);
        List<Map<String, Object>> data = getImportentData(queryMonth, positionId, id);
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 欠费数据信息
     *
     * @param queryMonth 查询日期
     * @param positionId 局编码
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/indicator/lackDataDetail", method = RequestMethod.GET)
    public String getLackData(String queryMonth, String positionId) throws JsonProcessingException {
        String id = getCon.queryIndexIdByIsImportent(2);
        List<Map<String, Object>> data = getImportentData(queryMonth, positionId, id);
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 单位菜单
     *
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/menu/detail", method = RequestMethod.GET)
    public String getMenuList() throws JsonProcessingException {
        List<Map<String, Object>> menuList = getCon.findOrgInfo(WDCODE, "8");
        List<Map<String, Object>> dataList = new ArrayList<>();
        List<Map<String, Object>> childreno = new ArrayList<>();
        Map<String, Object> gdj = new HashMap<>(16);
        for (Map<String, Object> map : menuList) {
            if (map.get("positionId").toString().length() == 6) {
                gdj.putAll(map);
                gdj.put(POSITIONLEVEL, "XJ");
            } else {
                map.put(POSITIONLEVEL, "GDS");
                List<Map<String, Object>> children = new ArrayList<>();
                List<Map<String, Object>> tqlist = getCon.findTQInfo(map.get("positionId").toString());
                for (Map<String, Object> tq : tqlist) {
                    tq.put(POSITIONLEVEL, "TQ");
                    tq.put(CHILDREN, new ArrayList<>());
                    children.add(tq);
                }
                map.put(CHILDREN, children);
                childreno.add(map);
            }
        }
        gdj.put(CHILDREN, childreno);
        dataList.add(gdj);
        return JsonUtil.objToJson(Result.success(dataList, "查询成功"));
    }

    /**
     * 搜索功能
     *
     * @param queryMonth 查询年月
     * @param queryType  年月类型，queryType=year按年查，否则按时间月查
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/indicator/search", method = RequestMethod.GET)
    public String getInfoByDateType(String queryMonth, String queryType) throws JsonProcessingException {

        List<Map<String, Map<String, String>>> data = new ArrayList<>();
        List<String> monthlist;
        String type = "year";
        if (comm.canFormat(queryMonth) == TRUE) {
            String qMonth = queryMonth.replace("-", "");
            if (type.equals(queryType)) {
                monthlist = getCon.findMonthOfYear(qMonth.substring(0, 4), null, "");
                for (String month : monthlist) {
                    Map<String, Map<String, String>> dataList = dataMap(month);
                    data.add(dataList);
                }
            } else {
                Map<String, Map<String, String>> datalist = dataMap(qMonth);
                data.add(datalist);
            }
        } else {
            monthlist = getCon.findAllMonth("");
            for (String month : monthlist) {
                Map<String, Map<String, String>> dataList = dataMap(month);
                data.add(dataList);
            }
        }
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 指标管理
     *
     * @param queryMonth 查询年月
     * @return
     */
    public Map<String, String> makeIndexManagement(String queryMonth) {
        Map<String, String> indexManagement = new HashMap<>(16);
        List<Map<String, String>> infoList = getCon.makeIndexManagement(queryMonth, WDCODE);
        int notPass = 0;

        for (Map<String, String> one : infoList) {
            if ("0".equals(one.get("isGetTarget"))) {
                notPass++;
            }
        }

        String qMonth = queryMonth.substring(0, 4).concat("-").concat(queryMonth.substring(4, 6));

        indexManagement.put("Incomplete", String.valueOf(notPass));
        indexManagement.put("accomplish", String.valueOf(15 - notPass));
        indexManagement.put("date", qMonth);
        if (infoList.isEmpty() == TRUE) {
            indexManagement.put(PROVINCERANKED, "0");
        } else {
            String code = infoList.get(0).get(ORGCODE);
            String count = (comm.getSynthesisRanked(qMonth, code) == null) ? "0" : comm.getSynthesisRanked(qMonth, code);
            indexManagement.put(PROVINCERANKED, count);
        }
        return indexManagement;
    }

    /**
     * 搜索结果集合
     *
     * @param queryMonth 查询时间
     * @return
     */
    public Map<String, Map<String, String>> dataMap(String queryMonth) {
        Map<String, Map<String, String>> data = new HashMap<>(16);
        List<Map<String, String>> list = getCon.findAllInfoByMonth(queryMonth, WDCODE);

        Map<String, String> lineLoss = new HashMap<>(16);
        Map<String, String> costData = new HashMap<>(16);
        for (Map<String, String> map : list) {
            if ("1".equals(String.valueOf(map.get(ISIMPORTENT)))) {
                lineLoss.put("id", map.get("id"));
                lineLoss.put(PROVINCERANKED, comm.getRanked(queryMonth, "1010", Double.parseDouble(map.get(COMPLETEVALUE)), "06", "0", "6"));
                lineLoss.put(COMPLETEVALUE, map.get(COMPLETEVALUE));
            }
            if ("2".equals(String.valueOf(map.get(ISIMPORTENT)))) {
                costData.put("id", map.get("id"));
                costData.put(COMPLETEVALUE, map.get(COMPLETEVALUE));
            }
        }
        Map<String, String> indexManagement = makeIndexManagement(queryMonth);

        data.put(INDEXMANAGEMENT, indexManagement);
        data.put("lineLoss", lineLoss);
        data.put("costData", costData);
        return data;
    }

    /**
     * 获取首页Map集合
     *
     * @param queryMonth 查询日期
     * @return
     */
    public Map<String, Object> allInfoMap(String queryMonth) {
        List<Map<String, String>> list = getCon.findAllInfoByMonth(queryMonth, WDCODE);
        Map<String, Object> datalist = new HashMap<>(16);
        List<Map<String, String>> particulars = new ArrayList<>();
        Map<String, Object> lineLoss = new HashMap<>(16);
        Map<String, Object> costData = new HashMap<>(16);

        if (list.isEmpty() == FALSE) {
            for (Map<String, String> map : list) {
                String indexId = map.get("id");
                Double completeValue = Double.parseDouble(map.get(COMPLETEVALUE));
                String orgCode = map.get(ORGCODE);
                String fx = String.valueOf(map.get("fx"));
                String length = String.valueOf(orgCode.length());
                String yearBasis = comm.getYBValue(queryMonth, map.get("id"), map.get(ORGCODE), null);
                String relativeRatio = comm.getRRValue(queryMonth, map.get("id"), map.get(ORGCODE), null);
                String provinceRanked = comm.getRanked(queryMonth, indexId, completeValue, orgCode.substring(0, 2), fx, length);
                String countyRanked = comm.getRanked(queryMonth, indexId, completeValue, orgCode.substring(0, 4), fx, length);
                String stationRanked = comm.getRanked(queryMonth, indexId, completeValue, orgCode.substring(0, 6), fx, length);
                map.put(PROVINCERANKED, provinceRanked);
                map.put(COUNTYRANKED, countyRanked);
                map.put(STATIONRANKED, stationRanked);
                map.put(YEARBASIS, yearBasis);
                map.put(RELATIVERATIO, relativeRatio);
                map.remove("fx");
                particulars.add(map);

                if ("1".equals(String.valueOf(map.get(ISIMPORTENT)))) {
                    lineLoss.put("id", map.get("id"));
                    lineLoss.put(PROVINCERANKED, provinceRanked);
                    lineLoss.put(COMPLETEVALUE, map.get(COMPLETEVALUE));
                }
                if ("2".equals(String.valueOf(map.get(ISIMPORTENT)))) {
                    costData.put("id", map.get("id"));
                    costData.put(COMPLETEVALUE, map.get(COMPLETEVALUE));
                }
            }
        } else {
            lineLoss.put("id", "undifined");
            lineLoss.put(TARGETVALUE, 0);
            lineLoss.put(PROVINCERANKED, 0);
            costData.put("id", "undifined");
            costData.put(TARGETVALUE, 0);
        }
        datalist.put("lineLoss", lineLoss);
        datalist.put("lackData", costData);
        datalist.put(PARTICULARS, particulars);

        return datalist;
    }

    /**
     * 获取详情页Map集合
     *
     * @param queryMonth 查询日期
     * @param positionId 单位编码
     * @param id         指标id
     * @return
     */
    public Map<String, Object> allInfoMapOfDetail(String queryMonth, String positionId, String id) {
        Map<String, Object> data = new HashMap<>(16);

        List<Map<String, String>> list = getCon.findIndexInfoByMonthAndId(queryMonth, id);
        if (list.isEmpty() == FALSE) {
            Map<String, String> currentData = getCon.findIndexInfoByMonthAndId(queryMonth, id).get(0);
            List<Map<String, String>> accomplishComp =
                    getCon.findInfoByDateAndOrgCode(queryMonth, positionId, id);

            Double completValue = Double.parseDouble(currentData.get(COMPLETEVALUE));
            String orgCode = currentData.get("code");
            String fx = String.valueOf(currentData.get("fx"));
            String length = String.valueOf(orgCode.length());

            String yearBasis = comm.getYBValue(queryMonth, id, currentData.get("code"), null);
            String relativeRatio = comm.getRRValue(queryMonth, id, currentData.get("code"), null);
            String provinceRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 2), fx, length);
            String countyRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 4), fx, length);
            String stationRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 6), fx, length);

            currentData.put(PROVINCERANKED, provinceRanked);
            currentData.put(COUNTYRANKED, countyRanked);
            currentData.put(STATIONRANKED, stationRanked);
            currentData.put(YEARBASIS, yearBasis);
            currentData.put(RELATIVERATIO, relativeRatio);

            currentData.remove("fx");

            for (Map<String, String> map : accomplishComp) {

                completValue = Double.parseDouble(map.get(COMPLETEVALUE));
                orgCode = map.get("code");
                fx = String.valueOf(map.get("fx"));
                length = String.valueOf(orgCode.length());

                yearBasis = comm.getYBValue(queryMonth, id, currentData.get("code"), null);
                relativeRatio = comm.getRRValue(queryMonth, id, currentData.get("code"), null);
                provinceRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 2), fx, length);
                countyRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 4), fx, length);
                stationRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 6), fx, length);

                map.put(PROVINCERANKED, provinceRanked);
                map.put(COUNTYRANKED, countyRanked);
                map.put(STATIONRANKED, stationRanked);
                map.put(YEARBASIS, yearBasis);
                map.put(RELATIVERATIO, relativeRatio);

                map.remove("fx");
                data.put(CURRENTDATA, currentData);
                data.put(ACCOMPLISHCOMP, accomplishComp);
            }
        } else {
            data.put(CURRENTDATA, "");
            data.put(ACCOMPLISHCOMP, "");
        }
        return data;
    }

    /**
     * 获取详情数据
     *
     * @param queryMonth 查询日期
     * @param id         指标id
     * @return
     */
    public Map<String, Object> allDetail(String queryMonth, String id) {
        Map<String, Object> data = new HashMap<>(16);
        if (getCon.findIndexInfoByMonthAndId(queryMonth, id).isEmpty() == FALSE) {
            Map<String, String> currentData = getCon.findIndexInfoByMonthAndId(queryMonth, id).get(0);
            Map<String, Map<String, String>> accomplishComp = new HashMap<>(16);

            List<Map<String, String>> particulars = new ArrayList<>(16);

            Double completValue = Double.parseDouble(currentData.get(COMPLETEVALUE));
            String orgCode = currentData.get("code");
            String fx = String.valueOf(currentData.get("fx"));
            String length = String.valueOf(orgCode.length());

            String yearBasis = comm.getYBValue(queryMonth, id, currentData.get("code"), null);
            String relativeRatio = comm.getRRValue(queryMonth, id, currentData.get("code"), null);
            String provinceRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 2), fx, length);
            String countyRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 4), fx, length);
            String stationRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 6), fx, length);

            currentData.put(YEARBASIS, yearBasis);
            currentData.put(RELATIVERATIO, relativeRatio);
            currentData.put(PROVINCERANKED, provinceRanked);
            currentData.put(COUNTYRANKED, countyRanked);
            currentData.put(STATIONRANKED, stationRanked);

            currentData.remove("fx");

            String year = queryMonth.substring(0, 4);
            List<String> monthList = getCon.findMonthOfYear(year, null, queryMonth);

            for (String month : monthList) {
                List<Map<String, String>> maplist = getCon.findIndexInfoByMonthAndId(month, id);
                for (Map<String, String> mapone : maplist) {

                    completValue = Double.parseDouble(mapone.get(COMPLETEVALUE));
                    orgCode = mapone.get("code");
                    length = String.valueOf(orgCode.length());

                    provinceRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 2), fx, length);
                    countyRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 4), fx, length);
                    stationRanked = comm.getRanked(queryMonth, id, completValue, orgCode.substring(0, 6), fx, length);

                    mapone.put(PROVINCERANKED, provinceRanked);
                    mapone.put(COUNTYRANKED, countyRanked);
                    mapone.put(STATIONRANKED, stationRanked);
                    mapone.put(YEARBASIS, yearBasis);
                    mapone.put(RELATIVERATIO, relativeRatio);

                    mapone.remove("fx");

                    particulars.add(mapone);
                }
            }


            Map<String, String> mounthAndBasis = mounthAndRY(queryMonth, currentData.get(COMPLETEVALUE), currentData.get(TARGETVALUE), "1");
            Map<String, String> mounthAndRatio = mounthAndRY(queryMonth, currentData.get(COMPLETEVALUE), currentData.get(TARGETVALUE), "");

            data.put(ACCOMPLISHCOMP, accomplishComp);
            data.put(PARTICULARS, particulars);
            data.put("mounthAndBasis", mounthAndBasis);
            data.put("mounthAndRatio", mounthAndRatio);
            data.put(CURRENTDATA, currentData);
        } else {
            data.put(ACCOMPLISHCOMP, "");
            data.put(PARTICULARS, "");
            data.put("mounthAndBasis", "");
            data.put("mounthAndRatio", "");
            data.put(CURRENTDATA, "");
        }
        return data;
    }

    /**
     * 计算同比环比
     *
     * @param queryMonth    日期
     * @param completeValue 完成值
     * @param targetValue   目标值
     * @param type          类型
     * @return
     */
    public Map<String, String> mounthAndRY(String queryMonth, String completeValue, String targetValue, String type) {
        Map<String, String> data = new HashMap<>(16);
        String targetTime;
        if (type == null || "".equals(type)) {
            String month = "01";
            String monthInDate = queryMonth.substring(4, 6);
            String targetDay = String.valueOf(Integer.parseInt(queryMonth.substring(4, 6)) - 1);
            targetTime = queryMonth.substring(0, 4) + "-" + targetDay;
            if (month.equals(monthInDate)) {
                targetTime = String.valueOf(Integer.parseInt(queryMonth.substring(0, 4)) - 1) + "-12";
            }
        } else {
            targetTime = String.valueOf(Integer.parseInt(queryMonth.substring(0, 4)) - 1) + "-" + queryMonth.substring(4, 6);
        }
        data.put("accomplish", completeValue);
        data.put(TARGETVALUE, targetValue);
        data.put("accomplishDate", queryMonth.substring(0, 4) + "-" + queryMonth.substring(4, 6));
        data.put("targetValueDate", targetTime);
        return data;
    }

    /**
     * 重要指标集合
     *
     * @param queryMonth 日期
     * @param positionId 单位编码
     * @param id         指标id
     * @return
     */
    public List<Map<String, Object>> getImportentData(String queryMonth, String positionId, String id) {
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, Object> datas;
        int length = 4;
        if (comm.canFormat(queryMonth) == TRUE) {
            if (queryMonth.length() > length) {
                String qMonth = queryMonth.replace("-", "");
                datas = allInfoMapOfDetail(qMonth, positionId, id);
                data.add(datas);
            } else {
                List<String> monthList = getCon.findMonthOfYear(queryMonth, id, "");
                for (String month : monthList) {
                    datas = allInfoMapOfDetail(month, positionId, id);
                    data.add(datas);
                }
            }
        } else {
            List<String> monthList = getCon.findAllMonth(id);
            for (String s : monthList) {
                if (getCon.findIndexInfoByMonthAndId(s, id).isEmpty() == FALSE) {
                    datas = allInfoMapOfDetail(s, positionId, id);
                    data.add(datas);
                    break;
                }
            }
        }
        return data;
    }
}


