package com.dingxin.wudang.board.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 生产看板和主人看板dao
 *
 * @author Ccccz
 */
public interface BoardDao {

    /**
     * Get Data For Board
     * 获取生产看板、主任看板指标数据
     *
     * @param witchBoard Board Number
     *                   看板编号
     * @return
     */
    List<Map<String, Object>> getMOPBoardQuota(@Param("porm") int witchBoard);


    /**
     * Get Quota's ID And Name For User
     * 获取指标基本信息根据角色
     *
     * @param realm User's Realm
     *              用户角色
     * @return
     */
    List<Map<String, Object>> getQuotaInfo(@Param("porm") int realm);

    /**
     * Get Order Number For Quota By Quota Id
     * 获取排序序号
     *
     * @param quotaId Quota Id
     *                指标id
     * @return
     */
    int getSortNum(@Param("quotaId") String quotaId);

    /**
     * Update New Order Number Start With The Quota's Order Number Which Is The Parameter
     * 更新数据库中除了作为参数的指标外其他指标的排序序号
     *
     * @param sort  Order Number Before This
     *              排序之前排序的序号
     * @param index The New Order Number For Quota
     *              需要排序后显示的位置
     * @param porm  Board Number
     *              看板编号
     * @return
     */
    int updateOtherSort(@Param("sort") int sort, @Param("index") int index, @Param("porm") int porm);

    /**
     * Update New Order Number By Quota Id
     * 根据指标的id更新序号
     *
     * @param map Include Quota Id And Order Number
     *            包含了指标id和排序编号
     * @return
     */
    int updateThisSort(@Param("map") Map map);

    /**
     * Get THe Latest Time For Quota
     * 获取最新指标时间
     *
     * @param porm Board Number
     *             看板编号
     * @return
     */
    String queryDateUpdate(@Param("porm") int porm);

    /**
     * Update Is Need This Quota To Show ON Board Index
     * 更改是否需要该指标展示到看板首页
     *
     * @param quotaId Quota Id
     *                指标id
     * @param onBoard Is Show On Board Index
     *                是否需要展示到看板首页
     * @return
     */
    int updateOnBoard(@Param("quotaId") String quotaId, @Param("onBoard") String onBoard);

    /**
     * Get Data From Somewhere Where Is Belong To Wudang By Quota Data's Time
     * 获取下级单位指标信息
     *
     * @param orgCode   Org Code
     *                  单位编码
     * @param quotaId   Quota Id
     *                  指标id
     * @param boardTime The Quota Time On Board Index
     *                  指标时间
     * @return
     */
    List<Map<String, Object>> getDeepDetails(@Param("orgCode") String orgCode, @Param("quotaId") String quotaId, @Param("boardTime") String boardTime);

    /**
     * Get Duty Person
     * 获取指标负责人
     *
     * @param quotaId Quota Id
     *                指标id
     * @return
     */
    String getDutyPerson(@Param("quotaId") String quotaId);

    /**
     * Get Board Number By Realm
     * 根据用户角色获取看板编号
     *
     * @param role User Realm
     *             用户角色
     * @return
     */
    int getPormByRole(@Param("role") String role);

    /**
     * Get CompleteValue And TargetValue Of Year
     * 获取一年中的完成值和目标值
     *
     * @param quotaId 指标id
     * @return
     */
    List<Map<String, Object>> getValueOfYear(@Param("quotaId") String quotaId, @Param("porm") String porm);
}
