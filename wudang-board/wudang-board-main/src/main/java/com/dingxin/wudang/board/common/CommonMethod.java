package com.dingxin.wudang.board.common;

import com.dingxin.wudang.board.dao.GetMobInfoDao;
import com.dingxin.wudang.board.dao.GetWebConnDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共方法类
 *
 * @author zhicheng.zhang
 */
@Configuration
public class CommonMethod {
    @Autowired
    GetWebConnDao getCon;

    @Autowired
    GetMobInfoDao getMobInfoDao;

    /**
     * 计算指标同比
     *
     * @param queryMonth 指标时间
     * @param indexid    指标id
     * @param orgCode    供电单位编码
     * @return
     */
    public String getYBValue(String queryMonth, String indexid, String orgCode, String type) {
        String cpDate = Integer.parseInt(queryMonth.substring(0, 4)) - 1 + queryMonth.substring(4, 6);
        Double nowCpValue = getCon.getCompleteValue(queryMonth, indexid, orgCode, type);
        Double lastCpvalue = getCon.getCompleteValue(cpDate, indexid, orgCode, type);
        if (lastCpvalue == null) {
            return "0";
        } else {
            BigDecimal ybValue = new BigDecimal(((nowCpValue - lastCpvalue) / lastCpvalue) * 100).setScale(2, BigDecimal.ROUND_HALF_UP);
            return ybValue.toString();
        }
    }

    public String getSynthesisRanked(String queryMonth, String orgCode) {
        queryMonth = queryMonth.replace("-", "");
        String completeValue = getCon.findAllCount(queryMonth, orgCode, String.valueOf(orgCode.length()), "");
        String synThesisRanked = getCon.findAllCount(queryMonth, orgCode.substring(0, orgCode.length() - 2), String.valueOf(orgCode.length()), completeValue);
        return synThesisRanked;
    }

    /**
     * 计算指标环比
     *
     * @param queryMonth 指标时间
     * @param indexid    指标id
     * @param orgCode    供电单位编码
     * @return
     */
    public String getRRValue(String queryMonth, String indexid, String orgCode, String type) {
        String rrDate = String.valueOf(Integer.parseInt(queryMonth) - 1);
        String month = "01";
        int start = 4;
        int end = 6;
        if (month.equals(queryMonth.substring(start, end))) {
            rrDate = String.valueOf(Integer.parseInt(queryMonth.substring(0, 4)) - 1) + "12";
        }
        Double nowCpValue = getCon.getCompleteValue(queryMonth, indexid, orgCode, type);
        Double lastCpValue = getCon.getCompleteValue(rrDate, indexid, orgCode, type);
        if (lastCpValue == null || nowCpValue == null || lastCpValue == 0.0) {
            return "0";
        } else {
            BigDecimal rrValue = new BigDecimal((nowCpValue - lastCpValue) / lastCpValue).setScale(2, RoundingMode.HALF_UP);
            return rrValue.toString();
        }
    }

    /**
     * 具体指标计算排名
     *
     * @param indexId       指标id
     * @param completeValue 完成值
     * @param rankCode      排名编码，取orgCode前两位，市取前4位，所取前6位
     * @param type          1.为正向指标，0.为负向指标
     * @param length        排名时候编码长度范围
     * @return
     */
    public String getRanked(String queryMonth, String indexId, Double completeValue, String rankCode, String type, String length) {
        List<String> value = Arrays.asList("2", "3");
        String getValue = getMobInfoDao.isInportent(indexId);
        if (value.contains(getValue)) {
            return "-";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("queryMonth", queryMonth);
        map.put("indexId", indexId);
        map.put("completeValue", completeValue);
        map.put("rankCode", rankCode);
        map.put("type", type);
        map.put("length", length);
        return getCon.findIndexCount(map);
    }

    /**
     * 判断字符串是否可以转换成日期
     *
     * @param queryMonth 日期字符串
     * @return
     */
    public boolean canFormat(String queryMonth) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        boolean convertSuccess = true;
        try {
            format.parse(queryMonth);
        } catch (ParseException e) {
            format = new SimpleDateFormat("yyyy");
            try {
                format.parse(queryMonth);
            } catch (ParseException pe) {
                convertSuccess = false;
            }
        }
        return convertSuccess;
    }


}
