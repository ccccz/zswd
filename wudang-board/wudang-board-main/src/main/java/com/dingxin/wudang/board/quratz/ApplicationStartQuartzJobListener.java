package com.dingxin.wudang.board.quratz;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 定时任务监听器
 *
 * @author zhicheng.zhang
 */
@Configuration
public class ApplicationStartQuartzJobListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private QuartzConfig quartzConfig;

    /**
     * 每天凌晨两点执行
     */
    private final static String EXECU = "0 0 2 * * ?";

    /**
     * 每5秒一次
     */
    private final static String EXECU1 = "*/10 * * * * ?";

    /**
     * 初始启动quartz
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
//        try {
//            quartzConfig.startJob(SchedulerJobOfCity.class, EXECU1, "city", "cityDate", "massive");
//            quartzConfig.startJob(SchedulerJobOfVillage.class, EXECU1, "village", "cityData", "massive");
//            scheduler().start();
//        } catch (SchedulerException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * 初始注入scheduler
     *
     * @return
     * @throws SchedulerException
     */
    @Bean
    public Scheduler scheduler() throws SchedulerException {
        SchedulerFactory schedulerFactoryBean = new StdSchedulerFactory();
        return schedulerFactoryBean.getScheduler();
    }
}