package com.dingxin.wudang.board.dao;

import com.dingxin.wudang.board.domain.Indexes;
import com.dingxin.wudang.board.domain.QuotaWeights;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface GetWebConnDao {

    /**
     * 信息录入
     *
     * @param indexes 指标信息对象
     * @return
     */
    int addIndexInfo(Indexes indexes);

    /**
     * 添加指标信息权重
     *
     * @param quotaWeights
     * @return
     */
    int addIndexWeights(QuotaWeights quotaWeights);

    /**
     * 查询是否重复添加数据
     *
     * @param indexId    指标id
     * @param queryMonth 查询时间
     * @param orgCode    单位编码
     * @param type       类型为quota查询指标信息，weights查询权重信息
     * @return
     */
    int duplicateData(@Param("id") String indexId, @Param("queryMonth") String queryMonth, @Param("orgCode") String orgCode, @Param("type") String type);

    /**
     * 更新权重
     *
     * @param quotaWeights
     * @return
     */
    int updateWeightsData(QuotaWeights quotaWeights);

    /**
     * 更新指标信息数据
     *
     * @param indexes
     * @return
     */
    int updateQuotaData(Indexes indexes);

    /**
     * 获取指定单位指定年月指定指标完成值
     *
     * @param queryMonth 查询时间
     * @param id         指标id
     * @param orgCOde    单位编码
     * @return
     */
    Double getCompleteValue(@Param("queryMonth") String queryMonth, @Param("indexId") String id, @Param("orgCode") String orgCOde, @Param("type") String type);

    /**
     * 查询重要指标的指标Id
     *
     * @param isImportent 重要指标标签
     * @return
     */
    String queryIndexIdByIsImportent(@Param("isImportent") int isImportent);

    /**
     * 获取某单位下供电单位名称
     *
     * @param orgCode 单位编码
     * @return
     */
    List<Map<String, String>> findOrgNameByOrgCode(@Param("orgCode") String orgCode);

    /**
     * 计算正向指标排名
     *
     * @param indexId       指标id
     * @param completeValue 完成值
     * @param rankCode      排名编码
     * @param type          排名类型
     * @param length        编码长度
     * @return
     */
    String findIndexCount(@Param("map") Map<String, Object> map);

    /**
     * 计算综合排名
     *
     * @param queryMonth   查询时间
     * @param orgCode      单位编码
     * @param length       编码长度
     * @param completValue 完成值
     * @return
     */
    String findAllCount(@Param("queryMonth") String queryMonth, @Param("prevOrgCode") String orgCode, @Param("length") String length, @Param("type") String completValue);

    /**
     * 指标详情面板信息
     *
     * @param queryMonth 查询年月
     * @param orgCode    单位编码
     * @return
     */
    List<Map<String, String>> makeIndexManagement(@Param("queryMonth") String queryMonth, @Param("orgCode") String orgCode);

    /**
     * 根据年月和指标id查询WEB端需要展示的指标，id为空默认查询年月下所有指标信息
     *
     * @param queryMonth 查询日期
     * @return
     */
    List<Map<String, String>> findAllInfoByMonth(@Param("queryMonth") String queryMonth, @Param("orgCode") String orgCode);

    /**
     * 获取乌当某个具体指标数据
     *
     * @param queryMounth 查询年月
     * @param id          指标Id
     * @return
     */
    List<Map<String, String>> findIndexInfoByMonthAndId(@Param("queryMonth") String queryMounth, @Param("id") String id);

    /**
     * 查询莫供电单位某段时间内某个具体指标数据
     *
     * @param queryMonth 查询日期
     * @param positionId 地市局编码
     * @param id
     * @return
     */
    List<Map<String, String>> findInfoByDateAndOrgCode(@Param("queryMonth") String queryMonth, @Param("positionId") String positionId, @Param("id") String id);

    /**
     * 查询某年中所有月份信息
     *
     * @param year 查询年份
     * @return
     */
    List<String> findMonthOfYear(@Param("year") String year, @Param("id") String id, @Param("queryMonth") String queryMonth);

    /**
     * 查询所有年月
     *
     * @return
     */
    List<String> findAllMonth(@Param("id") String id);

    /**
     * 通过指标id 查询指标名称
     *
     * @param checkMenu 指标id
     * @return
     */
    Map<String, String> findIndexNameByCheckMenu(@Param("check") String checkMenu);

    /**
     * 查询供电局和供电所基本信息
     *
     * @param orgCode
     * @return
     */
    List<Map<String, Object>> findOrgInfo(@Param("orgCode") String orgCode, @Param("length") String length);

    /**
     * 获取供电所下台区
     *
     * @param prevOrgCode 供电所编码
     * @return
     */
    List<Map<String, Object>> findTQInfo(@Param("prevOrgCode") String prevOrgCode);
}
