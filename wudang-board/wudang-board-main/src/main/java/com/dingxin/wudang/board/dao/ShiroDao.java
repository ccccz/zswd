package com.dingxin.wudang.board.dao;

import org.apache.ibatis.annotations.Param;

/**
 * @classname: ShiroDao
 * @author: Ccccz
 * @date: 2018/11/6 9:19
 */
public interface ShiroDao {

    String getPasswordByUname(@Param("uname") String uName);

    String getRole(@Param("uname") String uName);

    String getPermission(@Param("uname") String uName);
}
