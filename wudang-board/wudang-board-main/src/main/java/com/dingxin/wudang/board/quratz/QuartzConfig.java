package com.dingxin.wudang.board.quratz;


import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * 定时器功能和配置
 *
 * @author zhicheng.zhang
 */
@Configuration
public class QuartzConfig {
    /**
     * 任务调度
     */
    @Autowired
    private Scheduler scheduler;

    /**
     * 通过工作单元获取一个触发器
     *
     * @param job               工作单元
     * @param executionInterval 定时设置
     * @return
     * @throws ParseException
     */
    private CronTrigger getTrigger(JobDetail job, String executionInterval) {
        CronTrigger tigger = newTrigger()
                .withSchedule(cronSchedule(executionInterval))
                .forJob(job.getKey())
                .build();
        return tigger;
    }

    /**
     * 设置获取工作单元
     *
     * @param clazz    任务类
     * @param name     名称
     * @param group    分组
     * @param identity 身份
     * @return
     */
    private JobDetail setJob(Class clazz, String name, String group, String identity) {
        JobDetail jobDetail = newJob(clazz)
                .withIdentity(name, group)
                .build();
        return jobDetail;
    }


    /**
     * 执行一个定时任务
     *
     * @param clazz             任务类
     * @param executionInterval 执行定时约束
     * @param identity          身份
     * @param name              任务名称
     * @param group             任务分组
     * @throws SchedulerException
     */
    public void startJob(Class clazz, String executionInterval, String identity, String name, String group) throws SchedulerException {
        JobDetail jobDetail = setJob(clazz, name, group, identity);
        Trigger trigger = getTrigger(jobDetail, executionInterval);
        scheduler.scheduleJob(jobDetail, trigger);
    }

}
