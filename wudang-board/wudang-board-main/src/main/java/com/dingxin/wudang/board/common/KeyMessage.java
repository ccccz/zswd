package com.dingxin.wudang.board.common;

/**
 * Json格式字符串键值
 *
 * @author Ccccz
 * @date 2018-11-27
 */
public class KeyMessage {
    /**
     * 看板 返回成功字符串
     */
    public static final String GLOBLE_VALUE_SUCCESS_MSG = "SUCCESS";

    /**
     * 看板 是否把数据展示到看板首页
     */
    public static final String BOARD_KEY_ON_BOARD = "onBoard";

    /**
     * 看板 该条数据的时间
     */
    public static final String BOARD_KEY_BOARD_TIME = "boardTime";

    /**
     * 看板 返回的数据Id
     */
    public static final String BOARD_KEY_QUOTA_ID = "quotaId";

    /**
     * 看板 组织单位编码
     */
    public static final String BOARD_KEY_ORG_CODE = "org_code";

    /**
     * 看板 用户角色
     */
    public static final String BOARD_KEY_USER_ROLE = "role";

    /**
     * 看板 排序序号
     */
    public static final String BOARD_KEY_SORT_NUMBER = "sort";

    /**
     * 看板 判断当前指标信息是否达标的标志
     */
    public static final String BOARD_KEY_IS_GET_TARGET = "isGetTarget";

    /**
     * 看板 未达标标识
     */
    public static final String BOARD_VALUE_NOT_PASS = "0";

    /**
     * 看板 乌当下级单位数据
     */
    public static final String BOARD_KEY_SUB_ORG_DATA = "deepDetail";

    /**
     * 看板 该指标的负责人
     */
    public static final String BOARD_KEY_DUTY_PERSON = "dutyPerson";

    /**
     * 看板 指标综合排名
     */
    public static final String BOARD_KEY_TOTAL_RANK = "sourceNumber";

    /**
     * 看板 指标未达标数
     */
    public static final String BOARD_KEY_NOT_PASS_NUM = "notPass";

    /**
     * 看板 乌当指标数据信息
     */
    public static final String BOARD_KEY_ORG_DATA = "detail";

    /**
     * 看板 是否是最新数据
     */
    public static final String BOARD_KEY_IS_NEW_DATA = "isNew";

    /**
     * 看板 指标省排名
     */
    public static final String BOARD_KEY_PROVINCE_RANK = "pRanked";

    /**
     * 看板 指标市排名
     */
    public static final String BOARD_KEY_CITY_RANK = "cRanked";

    /**
     * 看板 指标县排名
     */
    public static final String BOARD_KEY_STATION_RANK = "sRanked";

    /**
     * 看板 是否展示到看板标识
     */
    public static final String BOARD_KEY_IS_SHOW = "isShow";

    /**
     * 看板 是否生产看板数据
     */
    public static final String BOARD_KEY_IS_PRODUCER_BOARD = "isProduceBroad";

    /**
     * 看板 是否展示省排名
     */
    public static final String BOARD_KEY_PROVINCE_RANK_SHOW = "provinceRankedShow";

    /**
     * 看板 是否展示市排名
     */
    public static final String BOARD_KEY_CITY_RANK_SHOW = "countyRankedShow";

    /**
     * 看板 是否展示县排名
     */
    public static final String BOARD_KEY_STATION_RANK_SHOW = "stationRankedShow";

    /**
     * 企业概况 变电站容量，变压器容量，线路长度，用户售电量
     */
    public static final String COMPANY_KEY_COMPANY_SIZE = "size";

    /**
     * 企业概况 变电站数量，变压器数量，线路数量，用户数量
     */
    public static final String COMPANY_KEY_COMANY_NUM = "number";

    /**
     * 企业概况 标题名称
     */
    public static final String COMPANY_KEY_TITLE_NAME = "titleName";

    /**
     * 企业概括 标题列表
     */
    public static final String COMPANY_KEY_TITLE_LIST = "titleList";

    /**
     * 企业概括 数据详情
     */
    public static final String COMPANY_KEY_DATAS = "datas";

    /**
     * 企业概括 数据标题列表
     */
    public static final String COMPANY_KEY_MENU_LIST = "menuList";

    /**
     * 企业概括 单位编码
     */
    public static final String COMPANY_KEY_ORG_CODE = "orgCode";

    /**
     * 企业概括 指标id
     */
    public static final String COMPANY_KEY_TYPE_ID = "typeid";

    /**
     * 企业概括 是否展示详情
     */
    public static final String COMPANY_KEY_IS_SHOW = "isShow";

    /**
     * 企业概括 供电所级数据
     */
    public static final String COMPANY_KEY_PLACE_DATA = "placeData";

    /**
     * 企业概括 变电站 值
     */
    public static final String COMPANY_VALUE_STATION = "station";

    /**
     * 企业概括 线路 值
     */
    public static final String COMPANY_VALUE_Line = "line";

    /**
     * 企业概括 变压器 值
     */
    public static final String COMPANY_VALUE_TRANSFROMER = "transformer";

    /**
     * 企业概括 用户 值
     */
    public static final String COMPANY_VALUE_USER = "user";
}
