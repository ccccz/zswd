package com.dingxin.wudang.board.controller;

import com.dingxin.wudang.board.common.Common;
import com.dingxin.wudang.board.common.CommonMethod;
import com.dingxin.wudang.board.dao.GetMobInfoDao;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 领导看板详情页
 *
 * @author zhicheng.zhang
 */
@RestController
@RequestMapping(value = "/getInfo")
@CrossOrigin
public class MobController {


    @Autowired
    GetMobInfoDao getInfoDao;

    @Autowired
    CommonMethod comm;

    @Autowired
    Common common;

    private static final String ORGCODE = "orgCode";
    private static final String COMPLETEVALUE = "completeValue";
    private static final String ISINPORTENT = "isInportent";
    private static final String PROVINCERANKED = "provinceRanked";
    private static final String ISGETTARGET = "isGetTarget";
    private static final String ORGID = "orgid";
    private static final String TARGETVALUE = "targetValue";
    private static final String DATAINDEXS = "dataIndex";
    private static final String ISJOINTARGET = "isJoinTarget";
    private static final String SIGN = "sign";
    private static final boolean TRUE = true;
    private static final boolean FALSE = false;


    /**
     * 指标详情展示
     *
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/getMobInfo1")
    public String getSubBoard(@RequestBody Map<String, String> map) throws JsonProcessingException {

        //获取看板编号
        int porm = common.getPorm(map.get("role"));
        //获取今年有数据的月份
        List<String> dateList = getInfoDao.queryDateOfYear();

        //一年中的完成值和目标值
        Map<String, Map<String, Object>> value = common.quotaValueOfYear(porm);

        //封装数据的集合
        List<Object> dataAll = new ArrayList<>();

        for (int i = 0, len = dateList.size(); i < len; i++) {
            //获取本次循环的月份
            String month = dateList.get(i);

            //获取该月的数据
            List<Map<String, Object>> list = getInfoDao.showMobInfo(month, String.valueOf(porm));

            Map<String, Object> datalist = new HashMap<>(16);

            int notPass = 0;

            Map<String, Object> mapv = getParticulars(list, notPass, value);
            List<Map<String, Object>> particulars = (List<Map<String, Object>>) mapv.get("particulars");

            boolean isNew = (i == (dateList.size() - 1)) ? TRUE : FALSE;
            Map<String, Object> indexManagement = getIndexMan("1", month, isNew, i, Integer.parseInt(mapv.get("notPass").toString()));
            datalist.put("indexManagement", indexManagement);
            datalist.put("particulars", particulars);
            datalist.put(ORGCODE, "060101");
            datalist = getAllMap(datalist);
            dataAll.add(datalist);
        }
        return JsonUtil.objToJson(Result.success(dataAll, "Success"));

    }

    private Map<String, Object> getParticulars(List<Map<String, Object>> list, int notPass, Map<String, Map<String, Object>> value) {

        Map<String, Object> map = new HashMap<>();

        List<Map<String, Object>> particulars = new ArrayList<>(16);

        for (int i = 0, len = list.size(); i < len; i++) {
            //获取该月中一条指标的数据信息
            Map<String, Object> mapone = list.get(i);

            //是否是欠费数据标识
            boolean isCast = false;

            //是否是重要指标标识
            boolean isInportent = false;

            mapone.putAll(value.get(mapone.get("quotaId")));

            notPass = (mapone.get("isGetTarget").toString().equals("1")) ? notPass : notPass++;

            //默认不打开详细信息
            mapone.put("isShow", false);

            notPass = replaceMap(mapone, notPass);

            mapone.put(PROVINCERANKED, 1);
            mapone.put("countyRanked", 1);
            mapone.put("stationRanked", 1);
            mapone.put(ISINPORTENT, isInportent);
            mapone.put("isCast", isCast);
            mapone = getSignValue(mapone);
            mapone.put("placeData", new ArrayList<>());
            particulars.add(mapone);
        }
        map.put("particulars", particulars);
        map.put("notPass", notPass);
        return map;
    }


  /*  @RequestMapping(value = "/getMobInfo", method = RequestMethod.GET)
    public String getMobShowInfo() throws JsonProcessingException {
        List<String> datelist = getInfoDao.queryDateOfYear();
        List<Object> dataAll = new ArrayList<>();
        for (int i = 0; i < datelist.size(); i++) {
            String queryMonth = datelist.get(i);
            List<Map<String, Object>> list = getInfoDao.showMobInfo(queryMonth);
            List<Map<String, Object>> particulars = new ArrayList<>(16);
            Map<String, Object> datalist = new HashMap<>(16);
            int notPass = 0;
            String year = queryMonth.substring(0, 4);
            for (Map<String, Object> mapone : list) {
                boolean isCast = FALSE;
                boolean isInportent = FALSE;
                mapone.put("isShow", FALSE);
                String orgCode = mapone.get(ORGCODE).toString();
                String indexid = mapone.get("id").toString();
                Double completValue = Double.parseDouble(mapone.get(COMPLETEVALUE).toString());
                String length = String.valueOf(orgCode.length());
                String fx = mapone.get("fx").toString();

                List<Map<String, Double>> v = getInfoDao.crValueOfYear(year, mapone.get("id").toString(), mapone.get(ORGCODE).toString());
                Map<String, Object> valueOfyear = valueOFYear(v);
                mapone.putAll(valueOfyear);
                notPass = replaceMap(mapone, notPass);
                mapone.put(PROVINCERANKED, comm.getRanked(queryMonth, indexid, completValue, orgCode.substring(0, 2), fx, length));
                mapone.put("countyRanked", comm.getRanked(queryMonth, indexid, completValue, orgCode.substring(0, 2), fx, length));
                mapone.put("stationRanked", comm.getRanked(queryMonth, indexid, completValue, orgCode.substring(0, 2), fx, length));
                mapone.put(ISINPORTENT, isInportent);
                mapone.put("isCast", isCast);
                mapone = getSignValue(mapone);
                mapone.put("placeData", new ArrayList<>());
                particulars.add(mapone);
            }
            boolean isNew = (i == (datelist.size() - 1)) ? TRUE : FALSE;
            Map<String, Object> indexManagement = getIndexMan(comm.getSynthesisRanked(queryMonth, list.get(0).get(ORGCODE).toString()), queryMonth, isNew, i, notPass);
            datalist.put("indexManagement", indexManagement);
            datalist.put("particulars", particulars);
            datalist.put(ORGCODE, "060101");
            datalist = getAllMap(datalist);
            dataAll.add(datalist);
        }
        return JsonUtil.objToJson(Result.success(dataAll, "查询成功"));
    }
*/

    /**
     * 获取重要指标集合
     *
     * @param map
     * @return
     */
    private Map<String, Object> getAllMap(Map<String, Object> map) {
        Map<String, Object> data = new HashMap<>(16);
        data.putAll(map);
        String lineLoss = "lineLoss";
        String costData = "costData";
        if (data.get(lineLoss) == null) {
            data.put(lineLoss, new HashMap<>(16));
        }
        if (data.get(costData) == null) {
            data.put(costData, new HashMap<>(16));
        }
        return data;
    }

    /**
     * 指标集合
     *
     * @param synthesisRanked 综合排名
     * @param date            时间
     * @param isNew           是否最新
     * @param dataindex       数据id
     * @param incomplete      未通过数
     * @return
     */
    private Map<String, Object> getIndexMan(String synthesisRanked, String date, boolean isNew, int dataindex, int incomplete) {
        Map<String, Object> indexManagement = new HashMap<>(16);
        indexManagement.put("provinceRankedShow", TRUE);
        indexManagement.put("countyRankedShow", FALSE);
        indexManagement.put("stationRankedShow", FALSE);
        indexManagement.put("synthesisRanked", synthesisRanked);
        indexManagement.put("date", date);
        indexManagement.put("isNew", isNew);
        indexManagement.put("dataindex", dataindex);
        indexManagement.put("Incomplete", incomplete);
        return indexManagement;
    }

    /**
     * 获取供电所和台区的数据
     *
     * @param queryMonth 查询日期
     * @param id         指标编号
     * @param orgCode    地市局编号
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/getMobGdsInfo", method = RequestMethod.GET)
    public String getGdsInfo(String queryMonth, String id, String orgCode) throws JsonProcessingException {
        List<Map<String, Object>> gdsListMap;
        List<Object> gdsdata = new ArrayList<>();
        String gdsindexid;
        gdsListMap = getInfoDao.queryGDS(orgCode);
        for (Map<String, Object> map : gdsListMap) {
            List<Map<String, Object>> gdslist = getInfoDao.queryGDSData(queryMonth, map.get(ORGID).toString(), id);
            for (Map<String, Object> gdsmap : gdslist) {
                String dataIndex = comm.getRanked(queryMonth, id, Double.parseDouble(gdsmap.get("completeValue").toString()), orgCode.substring(0, 6), String.valueOf(gdsmap.get("fx")), String.valueOf(orgCode.length() + 2));
                gdsindexid = gdsmap.get("id").toString();

                if (id.equals(gdsindexid)) {
                    String targetSign = gdsmap.get(TARGETVALUE).toString();
                    int isImportent = 2;
                    String completeSign;
                    if (Integer.parseInt(gdsmap.get("isImportent").toString()) == isImportent) {
                        completeSign = gdsmap.get(COMPLETEVALUE).toString();
                    } else {
                        completeSign = gdsmap.get(COMPLETEVALUE).toString() + gdsmap.get(SIGN);
                    }
                    map.put(DATAINDEXS, String.valueOf(dataIndex));
                    map.put(COMPLETEVALUE, completeSign);
                    map.put(TARGETVALUE, targetSign);
                    gdsdata.add(map);
                }
            }
        }

        return JsonUtil.objToJson(Result.success(gdsdata, "查询成功"));
    }

    /**
     * 台区指标
     *
     * @param queryMonth 年月
     * @param id         指标id
     * @param orgCode    供电单位编码
     * @return
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/getMobTqInfo", method = RequestMethod.GET)
    public String getTqInfo(String queryMonth, String id, String orgCode, String page) throws JsonProcessingException {
        List<Map<String, Object>> data = new ArrayList<>();
        int start = Integer.parseInt(page) * 20;
        Map<String, Object> param = new HashMap<>(16);
        param.put("queryMonth", queryMonth);
        param.put(ORGCODE, orgCode);
        param.put("id", id);
        param.put("page", start);
        List<Map<String, Object>> dataMap = getInfoDao.queryTQData(param);
        for (Map<String, Object> mapone : dataMap) {

            String dataindex = comm.getRanked(queryMonth, id, Double.parseDouble(mapone.get(COMPLETEVALUE).toString()), orgCode.substring(0, 2), null, String.valueOf(mapone.get(ORGID).toString().length()));
            mapone.put("dataindex", dataindex);
            String targetSign = mapone.get(TARGETVALUE).toString();
            String isImportent = "2";
            String completeSign;
            if (mapone.get("isImportent").equals(isImportent)) {
                completeSign = mapone.get(COMPLETEVALUE).toString();
            } else {
                completeSign = mapone.get(COMPLETEVALUE).toString() + mapone.get(SIGN);
            }
            mapone.put(COMPLETEVALUE, completeSign);
            mapone.put(TARGETVALUE, targetSign);
            data.add(mapone);
        }
        return JsonUtil.objToJson(Result.success(data, "查询成功"));
    }

    /**
     * 获取加单位的完成值和目标值
     *
     * @param mapone 指标信息
     * @return
     */
    public Map<String, Object> getSignValue(Map<String, Object> mapone) {
        Map<String, Object> map = mapone;
        String t = "-";
        String completeSign = map.get(COMPLETEVALUE).toString() + map.get(SIGN).toString();
        if (t.equals(map.get(TARGETVALUE).toString())) {
            map.put(TARGETVALUE, map.get(TARGETVALUE).toString());
        } else {
            map.put(TARGETVALUE, map.get(TARGETVALUE).toString() + map.get(SIGN).toString());
        }
        map.put(COMPLETEVALUE, completeSign);
        return map;
    }

    /**
     * 获取重要指标
     *
     * @param mapone     指标信息集合
     * @param queryMonth 查询时间
     * @return
     */
   /* public Map<String, Object> getInpotentIndex(Map<String, Object> mapone, String queryMonth) {

        String targetValue = mapone.get(TARGETVALUE).toString();
        String complete = mapone.get(COMPLETEVALUE).toString();
        Map<String, Object> map = new HashMap<>(16);
        String year = queryMonth.substring(0, 4);
        if (targetValue == null || targetValue == "" || "".equals(targetValue)) {
            targetValue = "-";
        }
        String target = ISGETTARGET;
        String value = "0";
        boolean isGetTarget = TRUE;
        if (value.equals(mapone.get(target).toString())) {
            isGetTarget = tof("");
        }
        List<Map<String, Double>> list = getInfoDao.crValueOfYear(year, mapone.get("id").toString(), mapone.get(ORGCODE).toString());
        Map<String, Object> dataMap = valueOFYear(list);
        map.putAll(dataMap);
        map.put(ISGETTARGET, isGetTarget);
        map.put("accomplish", complete);
        map.put("id", mapone.get("id"));
        map.put(TARGETVALUE, targetValue);

        return map;
    }*/

    /**
     * 将字符串转换为Boolean值
     *
     * @param flag 判断标签
     * @return
     */
    public boolean tof(String flag) {
        return ("true".equalsIgnoreCase(flag)) ? TRUE : FALSE;
    }

    /**
     * 获取一年中完成值和目标值
     *
     * @param listData 指标信息集合
     * @return
     */
    public Map<String, Object> valueOFYear(List<Map<String, Double>> listData) {
        Map<String, List<Double>> dataMap = new HashMap<>(16);
        Map<String, Object> map = new HashMap<>(16);
        List<Double> completeValue = new ArrayList<>(16);

        List<Double> targetValue = new ArrayList<>(16);
        for (Map<String, Double> one : listData) {
            completeValue.add(one.get("complete"));
            targetValue.add(one.get("target"));
        }
        dataMap.put("complete", completeValue);
        dataMap.put("target", targetValue);
        map.put("valueOfyear", dataMap);
        return map;
    }

    /**
     * 判断当前指标是否达标
     *
     * @param mapone  当前指标信息map
     * @param notPass 未达标数目
     * @return
     */
    public int replaceMap(Map<String, Object> mapone, int notPass) {
        String isJoinTatget = "1";
        String failTarget = "2";
        int num = notPass;
        if (failTarget.equals(mapone.get(ISGETTARGET).toString()) && isJoinTatget.equals(mapone.get(ISJOINTARGET).toString())) {
            num++;
        }
        return num;
    }

    public List<Object> subList(List<Object> gdsdata) {
        try {
            int leng = gdsdata.size();
            for (int i = 0; i < gdsdata.size(); i++) {
                for (int j = 0; j < gdsdata.size(); j++) {
                    Map<String, Object> mapone = (Map<String, Object>) gdsdata.get(j);
                    int dataIndex = Integer.parseInt(mapone.get(DATAINDEXS).toString());
                    if (dataIndex == (i + 1)) {
                        gdsdata.set(i, mapone);
                        break;
                    }
                }
            }
            List<Object> listdata = gdsdata.subList(0, leng);
            return listdata;
        } catch (Exception e) {
            return gdsdata;
        }
    }
}
