package com.dingxin.wudang.board.shiro;

import com.dingxin.wudang.board.dao.ShiroDao;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @classname: BoardRealm
 * @author: Ccccz
 * @date: 2018/11/5 23:27
 */
public class BoardRealm extends AuthorizingRealm {

    @Autowired
    ShiroDao shiroDao;

    /**
     * 获取授权信息(授权认证)
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("授权方法");
        AuthorizationInfo info = new SimpleAuthorizationInfo();
        String username = SecurityUtils.getSubject().getPrincipal().toString();
        String role = shiroDao.getRole(username);
        String permission = shiroDao.getPermission(username);
        Set<String> roles = new LinkedHashSet<>(16);
        roles.add(role);
        Set<String> permissions = new LinkedHashSet<>(16);
        permissions.add(permission);
        ((SimpleAuthorizationInfo) info).setRoles(roles);
        ((SimpleAuthorizationInfo) info).setStringPermissions(permissions);
        System.out.println(roles + "----" + permissions);
        return info;
    }

    /**
     * 获取身份认证信息(身份认证)
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        System.out.println("认证方法");

        String password = shiroDao.getPasswordByUname(authenticationToken.getPrincipal().toString());

        try {
            AuthenticationInfo info = new SimpleAuthenticationInfo(authenticationToken.getPrincipal(), password, this.getName());
            return info;
        } catch (UnknownAccountException e) {
            System.out.println("username is undifined");
            return null;
        } catch (IncorrectCredentialsException e) {
            System.out.println("password erroy");
            return null;
        }
    }
}
