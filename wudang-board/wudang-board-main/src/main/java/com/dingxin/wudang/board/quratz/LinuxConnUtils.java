package com.dingxin.wudang.board.quratz;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import com.dingxin.wudang.board.utils.JsonUtil;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * Linux连接工具类
 *
 * @author zhicheng.zhang
 */
@Configuration
public class LinuxConnUtils {
    /**
     * Linux测试ipK8s_Node01
     */
    public final static String TEST_K8S_NODE01 = "10.164.146.42";

    /**
     * Linux测试ipK8s_Node02
     */
    public final static String TEST_K8S_NODE02 = "10.164.146.43";

    /**
     * Linux正式ipK8s_Node01
     */
    public final static String OFFICIAL_K8S_NODE01 = "10.164.146.139";

    /**
     * Linux正式ipK8s_Node01
     */
    public final static String OFFICIAL_K8S_NODE02 = "10.164.146.140";

    /**
     * 数据中心测试服务器
     */
    public final static String DATA_CENTER_TEST = "10.164.146.44";

    /**
     * 数据中心正式服务器
     */
    public final static String DATA_CENTER_FORMAL = "10.164.146.142";

    /**
     * Linux帐号
     */
    private final static String USERNAME = "root";

    /**
     * Linux密码
     */
    private final static String PASSWORK = "Gzdw@123";

    /**
     * Linux远程连接
     */
    private Connection conn = null;

    /**
     * Linux连接Session
     */
    private Session session = null;

    /**
     * 设置超时时间
     */
    private static final int TIME_OUT = 1000 * 5 * 60;

    /**
     * 获取默认编码UTF-8
     */
    private static final String CHARSET = Charset.defaultCharset().toString();

    /**
     * 拼接输出字符串
     *
     * @param inputStream 输入流
     * @return
     * @throws IOException
     */
    private String getOutString(InputStream inputStream) throws IOException {
        /**
         * 设置IO缓冲区
         */
        byte[] b = new byte[1024];

        /**
         * 输出字符串
         */
        StringBuilder sb = new StringBuilder();

        /**
         * 从inputStream中获取二进制流并存入数组b中
         */
        while (inputStream.read(b) != -1) {
            /**
             * 讲b转换成字符串，使用编码为默认编码,并拼接输出字符串
             */
            sb.append(new String(b, CHARSET));
        }

        return sb.toString();
    }

    public boolean login(String ip, int port) {
        /**
         * 建立java和Linux的连接
         */
        conn = new Connection(ip, port);
        try {
            /**
             * 连接指定ip
             */
            conn.connect();
            /**
             * 认证用户名密码
             */
            return conn.authenticateWithPassword(USERNAME, PASSWORK);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 登录Linux并执行shell
     *
     * @param ip
     * @param port
     * @param shell
     * @return
     */
    public String execShell(String ip, int port, String shell) {
        InputStream isMsg = null;
        String errStr, msgStr;
        try {
            if (login(ip, port) == Boolean.TRUE) {
                /**
                 * 打开一个会话
                 */
                session = conn.openSession();
                /**
                 * 该会话执行shell语句
                 */
                session.execCommand(shell);
                isMsg = new StreamGobbler(session.getStdout());
                msgStr = getOutString(isMsg);
                Map<String, Object> map = new HashMap<>(16);
                map.put("msgStr", msgStr);
                session.waitForCondition(ChannelCondition.EXIT_STATUS, TIME_OUT);
                return JsonUtil.objToJson(map);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (isMsg != null) {
                IOUtils.closeQuietly(isMsg);
            }
        }
        return "Fail To Execute The Shell Command !";
    }

    public String execShells(String shells) {
        InputStream isMsg = null;

        try {
            /**打开一个会话
             */
            session = conn.openSession();
            /**
             * 该会话执行shell语句
             */
            isMsg = new StreamGobbler(session.getStdout());
            Map<String, Object> map = new HashMap<>(16);

            session.waitForCondition(ChannelCondition.EXIT_STATUS, TIME_OUT);
            session.execCommand(shells);
            return JsonUtil.objToJson(map);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (isMsg != null) {
                IOUtils.closeQuietly(isMsg);
            }
        }
        return "Fail To Execute The Shell Command !";
    }


}


