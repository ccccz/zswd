package com.dingxin.wudang.board.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

/**
 * 提供用户认证和身份认证的方法
 *
 * @classname: ShiroUtils
 * @author: Ccccz
 * @date: 2018/11/6 9:30
 */
@Component
public class ShiroUtils {
    /**
     * 判断用户是否登陆成功及对应的权限
     *
     * @param userName 用户名
     * @param passWord 密码
     * @return
     */
    public int wathBoard(String userName, String passWord) {
        UsernamePasswordToken token = new UsernamePasswordToken(userName, passWord);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            if (subject.hasRole("produce")) {
                return 1;
            } else if (subject.hasRole("market")) {
                return 2;
            } else {
                return 0;
            }
        } catch (AuthenticationException e) {
            return 0;
        }
    }
}
