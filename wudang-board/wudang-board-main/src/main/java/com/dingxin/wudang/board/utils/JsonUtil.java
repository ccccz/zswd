package com.dingxin.wudang.board.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * JSON类工具
 *
 * @author zhicheng.zhang
 */
public class JsonUtil {
    /**
     * 将对象装换成JSON字符串
     *
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    public static String objToJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    /**
     * Json字符串转对象
     *
     * @param json      json字符串
     * @param className 需要转换成的对象类
     * @return
     * @throws IOException
     */
    public static <T> T jsonToObj(String json, Class<T> className) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, className);
    }
}
