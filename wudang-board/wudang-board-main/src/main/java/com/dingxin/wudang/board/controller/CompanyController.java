package com.dingxin.wudang.board.controller;

import com.dingxin.wudang.board.common.KeyMessage;
import com.dingxin.wudang.board.dao.CompanyDao;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 战线变户
 *
 * @author Ccccz
 * @date 2018-11-28
 */
@RestController
@RequestMapping("/mob")
@CrossOrigin
public class CompanyController {

    @Autowired
    CompanyDao companyDao;

    private static final int DEFAULT_SIZE = 16;

    @GetMapping(value = "/api/sltuinfo")
    public String showData() throws JsonProcessingException {
        Map<String, Object> map = setTitle();

        return JsonUtil.objToJson(Result.success(map, KeyMessage.GLOBLE_VALUE_SUCCESS_MSG));
    }

    /**
     * 封装标题，信息，详细数据到Map中
     *
     * @return
     */
    private synchronized Map<String, Object> setTitle() {
        //初始化Map集合封装返回数据
        Map<String, Object> map = new HashMap<>(DEFAULT_SIZE);

        //添加tilleList数据
        List<String> titleList = new ArrayList<>(DEFAULT_SIZE);
        titleList.add("站");
        titleList.add("线");
        titleList.add("变");
        titleList.add("户");

        //添加titleName数据
        String titleName = "乌当供电局，面积686平方公里，人口30余万，管辖10KV线路122条，线路总长1906.12米，用电用户12万户，年售电量15.33瓦时";

        //添加 datas数据
        List<Map<String, Object>> datas = getDatas();

        //封装 titleName,titleList,datas并返回
        map.put(KeyMessage.COMPANY_KEY_TITLE_NAME, titleName);
        map.put(KeyMessage.COMPANY_KEY_TITLE_LIST, titleList);
        map.put(KeyMessage.COMPANY_KEY_DATAS, datas);

        return map;
    }


    /**

     * 获取详细数据
     *
     * @return
     */
    private List<Map<String, Object>> getDatas() {
        //初始化dataList封装返回数据
        List<Map<String, Object>> dataList = new ArrayList<>(DEFAULT_SIZE);

        //获取变电站数据并封装
        Map<String, Object> station = packMap(KeyMessage.COMPANY_VALUE_STATION);

        //获取线路数据并封装
        Map<String, Object> line = packMap(KeyMessage.COMPANY_VALUE_Line);

        //获取变压器数据并封装
        Map<String, Object> transformer = packMap(KeyMessage.COMPANY_VALUE_TRANSFROMER);

        //获取用户数据并封装
        Map<String, Object> user = packMap(KeyMessage.COMPANY_VALUE_USER);

        //将封装好的数据添加到集合中
        dataList.add(station);
        dataList.add(line);
        dataList.add(transformer);
        dataList.add(user);

        return dataList;
    }

    /**
     * Package Data
     * 封装数据
     *
     * @param type
     *             对应的参数获取不同数据
     * @return
     */
    private Map<String, Object> packMap(String type) {

        //初始化封装数据集合
        Map<String, Object> returnMap = new HashMap<>(DEFAULT_SIZE);

        //初始化菜单列表
        List<String> menuList = new ArrayList<>(DEFAULT_SIZE);

        //获取相应数据
        List<Map<String, Object>> datas = packDetail(type);
        if (KeyMessage.COMPANY_VALUE_STATION.equals(type)) {
            menuList.add("类型");
            menuList.add("数量");
            menuList.add("容量");
        } else if (KeyMessage.COMPANY_VALUE_Line.equals(type)) {
            menuList.add("类型");
            menuList.add("线路条数");
            menuList.add("容量");
        } else if (KeyMessage.COMPANY_VALUE_TRANSFROMER.equals(type)) {
            menuList.add("类型");
            menuList.add("台区数量");
            menuList.add("容量");
        } else {
            menuList.add("用户类型");
            menuList.add("用户数量");
            menuList.add("售电量");
        }

        //变电站 台数，容量；线路 长度，条数；变压器 台数，容量；用户 人数 售电量 的总和
        Map<String, Object> numAndSize = getNumberAndSize(datas);

        //添加数据到returnMap
        returnMap.put(KeyMessage.COMPANY_KEY_MENU_LIST, menuList);
        returnMap.put(KeyMessage.COMPANY_KEY_DATAS, datas);
        returnMap.putAll(numAndSize);

        return returnMap;
    }

    /**
     * 获取数量和容量总和
     *
     * @param list
     * @return
     */
    private Map<String, Object> getNumberAndSize(List<Map<String, Object>> list) {

        //初始化返回封装数据的集合
        Map<String, Object> numAndSize = new HashMap<>(DEFAULT_SIZE);


        //初始化数量和容量
        int number = 0;
        double size = 0;
        for (int i = 0, len = list.size(); i < len; i++) {

            //获取其中一项指标的数据
            Map<String, Object> map = list.get(i);


            //获取指标数量字符串
            String numberValue = map.get(KeyMessage.COMPANY_KEY_COMANY_NUM).toString();
            String sizeValue = map.get(KeyMessage.COMPANY_KEY_COMPANY_SIZE).toString();


            //将去掉'.'及后面的'0'的字符串转成Int型并做加法,Size直接做加法
            number += Integer.parseInt(numberValue.substring(0, numberValue.indexOf('.')));
            size += Double.parseDouble(sizeValue);


            //将结果转成字符串类型并封装
            String mapNumber = String.valueOf(number);
            String mapSize = String.valueOf(size);

            numAndSize.put(KeyMessage.COMPANY_KEY_COMANY_NUM, mapNumber);
            numAndSize.put(KeyMessage.COMPANY_KEY_COMPANY_SIZE, mapSize);

        }
        return numAndSize;
    }

    /**

     * 获取封装详细数据
     *
     * @param type
     *             对应的参数获取不同数据
     * @return
     */
    private List<Map<String, Object>> packDetail(String type) {

        //初始化返回封装数据的集合
        List<Map<String, Object>> data = companyDao.getData(type);
        for (int i = 0, len = data.size(); i < len; i++) {

            //获取其中一个指标，并未其添加下级单位数据集合
            Map<String, Object> map = data.get(i);


            //该指标单位编码
            String orgCode = map.get(KeyMessage.COMPANY_KEY_ORG_CODE).toString();

            //该指标指标编码
            String typeid = map.get(KeyMessage.COMPANY_KEY_TYPE_ID).toString();


            //获取下级单位数据列表
            List<Map<String, Object>> detailList = companyDao.getSubData(orgCode, typeid, type);


            //封装数据
            map.put(KeyMessage.COMPANY_KEY_IS_SHOW, false);
            map.put(KeyMessage.COMPANY_KEY_PLACE_DATA, detailList);
        }
        return data;
    }

}
