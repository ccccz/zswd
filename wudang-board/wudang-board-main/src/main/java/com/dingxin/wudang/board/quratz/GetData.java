package com.dingxin.wudang.board.quratz;

import com.dingxin.wudang.board.utils.JsonUtil;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GetData {
    public static void main(String[] args) {
        System.out.println("启动");
        LinuxConnUtils linuxConnUtils = new LinuxConnUtils();
        String shell = new StringBuilder("curl 10.115.184.15:8031/hrdsproute/rest/queryVmsDeptVoltRatem?appId=szwd\\&ksrq=")
                .append("2018-07-01")
                .append("\\&jsrq=")
                .append("2018-11-22")
                .append("\\&dwbm=37\\&cnwbz=1")
                .toString();
        System.out.println(shell);

        System.out.println("正在请求");
        String data = linuxConnUtils.execShell(LinuxConnUtils.TEST_K8S_NODE01, 22, shell);
        System.out.println(data);
        System.out.println("请求成功，处理为json字符串");
        String tmp = StringEscapeUtils.unescapeJavaScript(data)
                .replace("\\", "")
                .replace("\"[", "[")
                .replace("]\"", "]")
                .replace("\"{", "{");
        String json = tmp.substring(0, tmp.length() - 2).trim().concat("}");
        try {
            Map<String, Object> map = JsonUtil.jsonToObj(json, Map.class);
            Map<String, Object> msgStr = (Map) map.get("msgStr");
            Map<String, Object> reply = (Map) msgStr.get("reply");
            int resCode = (int) reply.get("resCode");
            if (resCode == 10000) {
                List<Map<String, Object>> results = (List) msgStr.get("result");
                Iterator<Map<String, Object>> iterator = results.iterator();
                while (iterator.hasNext()) {
                    Map<String, Object> result = iterator.next();
                    String passRate = result.get("hgl").toString();
                    String commitDate = result.get("tjrq").toString().replace("-", "").substring(0, 6);
                    System.out.println(passRate);
                    System.out.println(commitDate);
                }
            }
        } catch (
                IOException e1)

        {
            e1.printStackTrace();
        }


    }
}
