package com.dingxin.wudang.board.utils;

import java.util.UUID;

/**
 * UUID工具类
 */
public class UUIDUtil {
    /**
     * 获取uuid
     * @return
     */
    public static  String  getUUID(){
        return UUID.randomUUID().toString().trim();
    }
}
