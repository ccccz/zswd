package com.dingxin.wudang.board.quratz;

import com.dingxin.wudang.board.dao.GetWebConnDao;
import com.dingxin.wudang.board.dao.QuratzDao;
import com.dingxin.wudang.board.domain.Indexes;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.board.utils.UUIDUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 城市电压合格率数据获取
 *
 * @author zhicheng.zhang
 */
public class SchedulerJobOfCity implements Job {

    private QuratzDao quratzDao = ApplicationContextHolder.getBean(QuratzDao.class);

    private GetWebConnDao getWebConnDao = ApplicationContextHolder.getBean(GetWebConnDao.class);

    private static final int RESCODE = 10000;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        String month = String.format("%02d", calendar.get(Calendar.MONTH));
        String today = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        String yesterday = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH) - 1);

        LinuxConnUtils linuxConnUtils = new LinuxConnUtils();

        //获取curl开始时间
        String startTime = year + "-" + month + "-" + yesterday;

        //获取curl结束时间
        String endTime = year + "-" + month + "-" + today;
        //拼接Shell命令
        String shell = new StringBuilder("curl 10.115.184.15:8031/hrdsproute/rest/queryVmsDeptVoltRatem?appId=szwd\\&ksrq=")
                .append(startTime)
                .append("\\&jsrq=")
                .append(endTime)
                .append("\\&dwbm=37\\&cnwbz=1")
                .toString();

        String data = linuxConnUtils.execShell(LinuxConnUtils.OFFICIAL_K8S_NODE01, 22, shell);

        String tmp = StringEscapeUtils.unescapeJavaScript(data)
                .replace("\\", "")
                .replace("\"[", "[")
                .replace("]\"", "]")
                .replace("\"{", "{");
        String json = tmp.substring(0, tmp.length() - 2).trim().concat("}");
        try {
            Map<String, Object> map = JsonUtil.jsonToObj(json, Map.class);
            Map<String, Object> msgStr = (Map) map.get("msgStr");
            Map<String, Object> reply = (Map) msgStr.get("reply");
            int resCode = (int) reply.get("resCode");
            if (resCode == RESCODE) {
                List<Map<String, Object>> results = (List) msgStr.get("result");
                Iterator<Map<String, Object>> iterator = results.iterator();
                while (iterator.hasNext()) {
                    Map<String, Object> result = iterator.next();
                    String passRate = result.get("hgl").toString();
                    String commitDate = result.get("tjrq").toString().replace("-", "").substring(0, 6);

                    String quotaid = quratzDao.getQuotaId(7);
                    String maxTime = quratzDao.getCommitDate(quotaid);

                    System.out.println("maxTime=" + maxTime);
                    System.out.println("commitDate=" + commitDate);
                    if (maxTime == null || commitDate.compareTo(maxTime) == 1) {
                        System.out.println("data=" + data);
                        Indexes indexes = new Indexes();
                        indexes.setId(UUIDUtil.getUUID());
                        indexes.setCompleteValue(passRate);
                        indexes.setQuotaId(quotaid);
                        indexes.setOrgCode("060101");
                        indexes.setPrevOrgCode("060101");
                        indexes.setQuotaTime(commitDate);
                        indexes.setTargetValue("99.9");

                        getWebConnDao.addIndexInfo(indexes);
                    }
                }
            }


        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
