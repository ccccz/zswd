package com.dingxin.wudang.board;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * @author : chuncheng.peng
 * @date: 2018-05-21 11:54
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@MapperScan(value = "com.dingxin.wudang.*.dao")
public class WudangBoardApplication {
    public static void main(String[] args) {
        SpringApplication.run(WudangBoardApplication.class, args);
    }
}
