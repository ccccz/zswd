package com.dingxin.wudang.board.common;

import com.dingxin.wudang.board.dao.BoardDao;
import com.dingxin.wudang.board.dao.CommonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Common Method
 * 公共方法
 *
 * @author Ccccz
 * @date 2018-11-26
 */
@Component
public class Common {

    @Autowired
    CommonDao commonDao;

    @Autowired
    BoardDao boardDao;

    /**
     * Calculated Ring Ratio
     * 计算环比
     *
     * @param quotaId   Quota ID
     *                  指标编码
     * @param orgCode   Org Code
     *                  单位编码
     * @param boardTime Quota Time On Board
     *                  指标时间
     * @return
     */
    public Map<String, Object> getRatioVlaue(String quotaId, String orgCode, String boardTime, int direction) {
        //获取计算环比和同比的数据信息
        List<String> completeValues = commonDao.getRingRatioData(quotaId, boardTime, orgCode);

        //获取当前完成值
        String currentCompleteValue = completeValues.get(0);
        double ccValue = Double.parseDouble(currentCompleteValue);


        //获取上个月完成值
        String lastMonthValue = completeValues.get(1);
        double lmValue = Double.parseDouble(lastMonthValue);


        //获取去年同一个月完成值
        String lastYearValue = completeValues.get(2);
        double lyValue = Double.parseDouble(lastYearValue);


        //计算同比
        double yrValue = (ccValue - lyValue) / lyValue;
        String yearRatio = String.valueOf(yrValue);
        boolean yearRatioIsImprove = ratioValueIsImprove(yrValue, direction);


        //计算环比
        double rrValue = (ccValue - lmValue) / lmValue;
        String ringRatio = String.valueOf(rrValue);
        boolean ringRatioIsImprove = ratioValueIsImprove(rrValue, direction);


        //封装返回
        Map<String, Object> map = new HashMap<>(16);

        map.put("yearRatio", yearRatio);
        map.put("yrIsUp", yearRatioIsImprove);

        map.put("ringRatio", ringRatio);
        map.put("rrIsUp", ringRatioIsImprove);
        return map;
    }

    /**
     * @param value
     * @param direction
     * @return
     */
    private boolean ratioValueIsImprove(double value, int direction) {
        //根据指标方向确认比值是否增长
        boolean flag = (1 == direction && value >= 0) || (0 == direction && value <= 0);
        return (flag) ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * 获取一年中完成值和目标值
     *
     * @param listData 指标信息集合
     * @return
     */
    public Map<String, Object> valueOFYear(List<Map<String, Object>> listData) {
        Map<String, List<Object>> dataMap = new HashMap<>(16);
        Map<String, Object> map = new HashMap<>(16);
        List<Object> completeValue = new ArrayList<>(16);

        List<Object> targetValue = new ArrayList<>(16);
        for (Map<String, Object> one : listData) {
            completeValue.add(one.get("complete"));
            targetValue.add(one.get("target"));
        }
        dataMap.put("complete", completeValue);
        dataMap.put("target", targetValue);
        map.put("valueOfyear", dataMap);
        return map;
    }


    /**
     * The Method To Get Num By Current Login User
     * 根据角色判断看板编号
     *
     * @param role User Realm
     *             用户角色
     * @return
     */
    public int getPorm(String role) {
        return boardDao.getPormByRole(role);
    }

    public List<Map<String, Object>> getVlaueOfYear(String quotaId, int porm) {
        return boardDao.getValueOfYear(quotaId, String.valueOf(porm));
    }

    public List<String> getQuotaList(int porm) {
        return commonDao.getQuotaId(porm);
    }

    /**
     * 获取并格式化一年中的完成者和目标值，用于展示折线图
     *
     * @param porm
     * @return
     */
    public Map<String, Map<String, Object>> quotaValueOfYear(int porm) {
        List<String> quotaList = getQuotaList(porm);
        Map<String, Map<String, Object>> value = new HashMap<>();
        for (int i = 0, len = quotaList.size(); i < len; i++) {
            String key = quotaList.get(i);
            //一年中的完成值和目标值，用于前段展示折线图
            List<Map<String, Object>> listData = getVlaueOfYear(key, porm);
            Map<String, Object> crList = valueOFYear(listData);
            value.put(key, crList);
        }
        return value;
    }

    /**
     * 根据年月和看板编号查询看板数据
     *
     * @param map
     * @return
     */
    public List<Map<String, Object>> getQuotaByMonth(Map map) {
        return commonDao.getQuotaByMonth(map);
    }

    /**
     * 获取看板时间
     *
     * @param porm
     * @return
     */
    public List<String> getMonthList(String porm) {
        return commonDao.getQueryMonth(porm);
    }
}
