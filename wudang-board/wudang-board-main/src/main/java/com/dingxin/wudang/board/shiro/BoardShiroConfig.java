package com.dingxin.wudang.board.shiro;

import com.dingxin.wudang.board.dao.ShiroDao;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @classname: BoardShiroConfig
 * @author: Ccccz
 * @date: 2018/11/5 23:31
 */
@Configuration
public class BoardShiroConfig {


    /**
     * 获取BoardRealm认证逻辑
     *
     * @return
     */
    @Bean
    public BoardRealm getRealm() {
        return new BoardRealm();
    }

    /**
     * 获取安全认证管理器
     *
     * @return
     */
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getSecurityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(getRealm());
        return securityManager;
    }

    /**
     * 获取Shiro过滤器
     *
     * @param securityManager
     * @return
     */
    @Bean(name = "shiroFilterFactoryBean")
    public ShiroFilterFactoryBean getShiroFileter(@Qualifier("securityManager") SecurityManager securityManager) {
        ShiroFilterFactoryBean factory = new ShiroFilterFactoryBean();
        factory.setSecurityManager(securityManager);

        Map<String, String> filterMap = new LinkedHashMap<>(16);
        // filterMap.put("/api/p&m/board/autoc", "anon");
        // filterMap.put("/api/p&m/board/**", "authc");
        //filterMap.put("/b/b","perms[market]");
        //filterMap.put("/b/b", "perms[marketquery]");
        // factory.setLoginUrl("localhsot:3575/b/a");

        factory.setFilterChainDefinitionMap(filterMap);

        return factory;
    }
}
