package com.dingxin.wudang.board.controller;


import com.dingxin.wudang.board.common.Common;
import com.dingxin.wudang.board.common.KeyMessage;
import com.dingxin.wudang.board.dao.BoardDao;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @classname: 看板首页接口
 * @author: Ccccz
 * @date: 2018/11/15 15:48
 */
@RestController
@RequestMapping("/api/wudang")
@CrossOrigin
public class BoardsController {

    @Autowired
    BoardDao boardDao;

    @Autowired
    Common common;


    /**
     * 展示数据
     *
     * @param map 包含了用户角色，指标id集合，是否展示到看板首页的map
     * @return
     */
    @PostMapping(value = "/board")
    public String getPersonBoard(@RequestBody Map<String, Object> map) throws JsonProcessingException {

        //从Map中获取当前用户的角色信息和quotaId
        String role = map.get(KeyMessage.BOARD_KEY_USER_ROLE).toString();
        List<String> quotaId = (List<String>) map.get(KeyMessage.BOARD_KEY_QUOTA_ID);

        //获取当前用户的板块编号
        int porm = common.getPorm(role);

        //获取设备主人看板数据时间列表
        List<String> monthList = common.getMonthList(String.valueOf(porm));

        //返回Json格式的List
        List<Object> returnList = new ArrayList<>();

        //一年中的完成值和目标值
        Map<String, Map<String, Object>> value = common.quotaValueOfYear(porm);

        //循环取出每月指标
        for (int i = 0, len = monthList.size(); i < len; i++) {
            //封装查询当前模块当月数据的参数
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("month", monthList.get(i));
            paramMap.put("porm", porm);

            List<Map<String, Object>> resultMap = common.getQuotaByMonth(paramMap);

            Map<String, Object> result = getUserData(resultMap, porm, value);
            result.put("index", i);
            result.put("boardTime", monthList.get(i));

            //判断是否最新月份数据
            boolean isNew = (i == len - 1) ? true : false;
            result.put("isNew", isNew);
            returnList.add(result);
        }

        if (!quotaId.isEmpty()) {
            boolean onBoard = (boolean) map.get(KeyMessage.BOARD_KEY_ON_BOARD);
            getOrderListByUser(quotaId, onBoard, porm);
        }

        return JsonUtil.objToJson(Result.success(returnList, "Success"));
    }

    /**
     * 对应看板指标清单
     *
     * @param role 用户角色
     * @return
     * @throws JsonProcessingException
     */
    @GetMapping(value = "/menu")
    public String showMenuList(String role) throws JsonProcessingException {

        //获取角色对应看板编号
        int porm = common.getPorm(role);


        //根据编号查询看板数据
        List<Map<String, Object>> data = boardDao.getQuotaInfo(porm);

        return JsonUtil.objToJson(Result.success(data, KeyMessage.GLOBLE_VALUE_SUCCESS_MSG));
    }


    private Map<String, Object> getUserData(List<Map<String, Object>> resultMap, int porm, Map<String, Map<String, Object>> value) {


        int notPass = 0;
        List<Map<String, Object>> quotaByMonth = new ArrayList<>();
        for (int i = 0, len = resultMap.size(); i < len; i++) {
            //获取当前月指标数据
            Map<String, Object> mapone = resultMap.get(i);

            //为当前月数据添加排名信息
            mapone.putAll(aboutRanked(mapone));
            quotaByMonth.add(mapone);

            //一年中的目标值和完成值
            mapone.putAll(value.get(mapone.get("id")));

            //下级单位指标数据信息
            //TODO
            mapone.put("deepDetail", new ArrayList<>());

            //获取目标值
            String isGetTarge = mapone.get("isGetTarget").toString();

            //判断是否达标，不达标，notPass+1
            notPass = (isGetTarge.equals("0")) ? notPass++ : notPass;

            if (1 == porm) {

                mapone.put(KeyMessage.BOARD_KEY_DUTY_PERSON, "王仕达");
                mapone.put("phone", "13888888888");


                //运检中心和个人下拉界面数据
                //TODO
                String quotaId = mapone.get("id").toString();

                mapone.putAll(getMenu(quotaId));

            }
        }
        Map<String, Object> returnMap = new HashMap<>(4);
        returnMap.put("notPass", notPass);
        returnMap.put("detail", quotaByMonth);

        //指标综合排名(按指标权重，指标方向计算)
        //TODO
        String sourceNumber = "1";
        returnMap.put("sourceNumber", sourceNumber);
        return returnMap;
    }

    /**
     * 按照用户选中对指标排序
     *
     * @param quotaIds 指标id的List集合
     * @param onBoard  是否将该指标展示到看板首页
     * @param porm     看板编号
     */
    private void getOrderListByUser(List<String> quotaIds, boolean onBoard, int porm) {
        for (int i = 0, len = quotaIds.size(); i < len; i++) {

            //获取指标id
            String quotaId = quotaIds.get(i);

            //获取一项指标自定义排序之前的序号
            int sortBefore = boardDao.getSortNum(quotaId);


            //根据指标id设置该指标是否展示到看板首页
            boardDao.updateOnBoard(quotaId, booleanToString(onBoard));


            //从数据库中更新指标展示顺序
            boardDao.updateOtherSort(sortBefore, i, porm);

            Map<String, Object> map = new HashMap<>(16);
            map.put(KeyMessage.BOARD_KEY_SORT_NUMBER, i);
            map.put(KeyMessage.BOARD_KEY_QUOTA_ID, quotaId);
            boardDao.updateThisSort(map);

        }

    }

    /**
     * 把Boolean类型转换String 类型
     *
     * @param bool
     * @return
     */
    private String booleanToString(boolean bool) {
        return String.valueOf(bool);
    }

    /**
     * 把String类型转换成oolean类型
     *
     * @param str 字符串"true" 或 "false"
     * @return
     */
    private boolean stringToBoolean(String str) {
        return Boolean.valueOf(str);
    }


    /**
     * 计算排名并添加其他相关信息到集合中
     *
     * @param mapDetail 按数据时间分块的数据
     * @return
     */
    private Map<String, Object> aboutRanked(Map<String, Object> mapDetail) {
        Map<String, Object> map = mapDetail;


        //计算排名
        //TODO
        int pRanked = 1;
        int cRanked = 1;
        int sRanked = 1;

        map.put(KeyMessage.BOARD_KEY_PROVINCE_RANK, pRanked);
        map.put(KeyMessage.BOARD_KEY_CITY_RANK, cRanked);
        map.put(KeyMessage.BOARD_KEY_STATION_RANK, sRanked);

        map.put(KeyMessage.BOARD_KEY_IS_SHOW, Boolean.FALSE);
        map.put(KeyMessage.BOARD_KEY_IS_PRODUCER_BOARD, Boolean.TRUE);


        //需要展示那个排名
        map.put(KeyMessage.BOARD_KEY_PROVINCE_RANK_SHOW, Boolean.TRUE);
        map.put(KeyMessage.BOARD_KEY_CITY_RANK_SHOW, Boolean.FALSE);
        map.put(KeyMessage.BOARD_KEY_STATION_RANK_SHOW, Boolean.FALSE);

        String onBoardStr = map.get(KeyMessage.BOARD_KEY_ON_BOARD).toString();
        boolean onBoard = stringToBoolean(onBoardStr);
        map.put(KeyMessage.BOARD_KEY_ON_BOARD, onBoard);

        return map;
    }

    /**
     * @param map 乌当局编码,指标id，数据时间
     * @return
     */
    private List<Map<String, Object>> getSubOrgData(Map<String, Object> map) {

        //获取当前单位编码
        String orgCode = map.get(KeyMessage.BOARD_KEY_ORG_CODE).toString();

        //获取指标id
        String quotaId = map.get(KeyMessage.BOARD_KEY_QUOTA_ID).toString();


        //获取看板数据时间
        String boardTime = map.get(KeyMessage.BOARD_KEY_BOARD_TIME).toString();
        return boardDao.getDeepDetails(orgCode, quotaId, boardTime);
    }

    /**
     * 主人看板运检模块和个人模块
     *
     * @param quotaId 当前指标id
     * @return
     */
    private static Map<String, List<String>> getMenu(String quotaId) {

        Map<String, List<String>> map = new HashMap<>();
        List<String> yjlisttype = new ArrayList<>();
        List<String> yjlisttypej = new ArrayList<>();
        List<String> grlisttype = new ArrayList<>();
        List<String> grlisttypej = new ArrayList<>();

        if (!"G_XB_GISSC_YZL_JCZL".equals(quotaId)) {
            //运检中心非基础资料准确率下拉列表数据
            yjlisttype.add("王世达");
            yjlisttype.add("1");

            //个人非基础资料准确率下拉列表数据
            grlisttype.add("王世达");
            grlisttype.add("王仕达");

        } else {
            //运检中心基础资料准确率下拉列表数据
            yjlisttypej.add("XX变压器");
            yjlisttypej.add("XX线路");
            yjlisttypej.add("XX变电站");
            yjlisttypej.add("王仕达");

            //个人基础资料准确率下拉列表数据
            grlisttypej.add("XX变压器");
            grlisttypej.add("XX线路");
            grlisttypej.add("XX变电站");
        }
        map.put("yjlisttype", yjlisttype);
        map.put("yjlisttypej", yjlisttypej);
        map.put("grlisttype", grlisttype);

        map.put("grlisttypej", grlisttypej);
        return map;

    }
}


