package com.dingxin.wudang.board.controller;


import com.dingxin.wudang.board.common.CommonMethod;
import com.dingxin.wudang.board.dao.GetWebConnDao;
import com.dingxin.wudang.board.domain.Indexes;
import com.dingxin.wudang.board.domain.QuotaWeights;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.isA;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebControllerTest {
    @InjectMocks
    private WebController webController;

    @Mock
    private CommonMethod comm;

    @Mock
    private GetWebConnDao getWebConnDao;

    private static List<String> menuItem;//指标别名测试集合
    private static List<String> menuName;//指标名称测试集合
    private static List<String> menuId;//指标id测试集合
    private static Map<String, String> data = new HashMap<>();//添加指标数据测试集合
    private static final List<String> month = Arrays.asList("2018-06", "2018", "2017-06", "2017");//输入时间的格式Matchers
    private static List<Map<String, String>> companyList = new ArrayList<>();//测试单位集合
    private static Map<String, String> weights = new HashMap<>();//权重测试数据集合
    private static final List<String> dataMonth = Arrays.asList("2018-01", "2018-02", "2018-03", "2018-04", "2018-05", "2018-06", "2018-07");//测试指标时间集合
    private static final List<String> allDate = Arrays.asList("201801", "201802", "201803", "201804", "201805", "201806", "201807");
    private static final List<String> completValue = Arrays.asList("91", "90", "95", "89", "87", "93", "97", "86", "87", "88", "80", "97", "91", "99", "93");//15项指标完成值
    private static final List<String> targetValue = Arrays.asList("91", "90", "95", "89", "87", "93", "97", "86", "87", "88", "87", "97", "91", "99");//14项指标目标值
    private static final List<String> direction = Arrays.asList("0", "1");//指标方向
    private static final List<String> importent = Arrays.asList("0", "1", "2");//重要指标标签
    private static final List<String> rankedOrg = Arrays.asList("06", "0601", "060101");//排名id
    private static List<Map<String, String>> datalistOfYear = new ArrayList<>();//某年中乌当指标测试数据
    private static List<Map<String, String>> datalistOfMonth = new ArrayList<>();//某月中乌当指标测试数据
    private static final List<String> matcherType = Arrays.asList(null, "");//查询完成值类型
    private static final List<String> length = Arrays.asList("2", "4", "6");//排名单位编码长度
    private static final Map<String, Object> allInfoMap = new HashMap<>();//allInfoMap测试数据(查询数据不为空)
    private static final Map<String, Object> allInfoEmpty = new HashMap<>();//allInfoMap测试数据(查询数据为空)
    private static final List<String> allInfoMonth = Arrays.asList("201802", "201801");//测试无“-”日期
    private static Map<String, String> wudangMap = new HashMap<>();//

    @BeforeClass
    public static void setUp() {
        //初始化allInfoMap测试数据
        List<Map<String, String>> particulars = new ArrayList<>();
        Map<String, Object> lineLoss = new HashMap<>();
        Map<String, Object> costData = new HashMap<>();
        Map<String, String> allinfo = new HashMap<>();
        allinfo.put("provinceRanked", "1");
        allinfo.put("countyRanked", "1");
        allinfo.put("stationRanked", "1");
        allinfo.put("yearBasis", "1.26");
        allinfo.put("relativeRatio", "2.36");
        particulars.add(allinfo);

        lineLoss.put("id", "1010");
        lineLoss.put("provinceRanked", "1");
        lineLoss.put("targetValue", "80");

        costData.put("id", "1015");
        costData.put("targetValue", "90");
        allInfoMap.put("lineLoss", lineLoss);
        allInfoMap.put("lackData", costData);
        allInfoMap.put("particulars", particulars);

        particulars = new ArrayList<>();
        lineLoss = new HashMap<>();
        costData = new HashMap<>();
        lineLoss.put("id", "undifined");
        lineLoss.put("targetValue", 0);
        lineLoss.put("provinceRanked", 0);
        costData.put("id", "undifined");
        costData.put("targetValue", 0);
        allInfoEmpty.put("lineLoss", lineLoss);
        allInfoEmpty.put("lackData", costData);
        allInfoEmpty.put("particulars", particulars);

        //初始化测试单位
        Map<String, String> map = new HashMap<>();
        map.put("code", "060101");
        map.put("name", "乌当供电局");
        companyList.add(map);
        map.put("code", "06010103");
        map.put("name", "新天供电所");
        companyList.add(map);
        map.put("code", "06010104");
        map.put("name", "东风供电所");
        companyList.add(map);
        map.put("code", "06010105");
        map.put("name", "羊昌供电所");
        companyList.add(map);
        map.put("code", "06010106");
        map.put("name", "水田供电所");
        companyList.add(map);
        map.put("code", "06010107");
        map.put("name", "下坝供电所");
        companyList.add(map);

        //初始化测试指标
        data.put("completeValue", "14334");
        data.put("targetValue", "34124");
        data.put("userName", "张三");
        data.put("stationName", "乌当");
        data.put("station", "060101");
        data.put("menuItem", "urban-residents");
        data.put("queryMonth", "2018-06");

        //初始化测试权重
        weights.put("form-entry", "0.3");
        weights.put("informatization", "0.2 ");
        weights.put("satisfaction", "0.03");
        weights.put("danger-rectification", "0.02");
        weights.put("equipment-defect", "0.2");
        weights.put("rural-voltage", "0.05 ");
        weights.put("outage-time", "0.05 ");
        weights.put("urban-residents", "0.05");
        weights.put("recovery-electric", "0.04");
        weights.put("loss-rate", "0.01");
        weights.put("business-income", "0.01");
        weights.put("controllable-cost", "0.01");
        weights.put("integrated-loss", "0.01");
        weights.put("data-integrity", "0.02");
        weights.put("queryMonth", "2018-06");

        //初始化测试指标id
        menuId = Arrays.asList("1001",
                "1002",
                "1003",
                "1004",
                "1005",
                "1006",
                "1007",
                "1008",
                "1009",
                "1010",
                "1011",
                "1012",
                "1013",
                "1014",
                "1015"
        );

        //初始化测试指标名称
        menuName = Arrays.asList("百万工时工伤意外率",
                "信息化水平",
                "第三方客户满意度",
                "隐患整改完成率",
                "设备消缺率",
                "农村居民端电压合格率",
                "客户平均停电时间",
                "城市居民端电压合格率",
                "电费回收率",
                "10千伏以下有损线损率",
                "主营业务收入",
                "单位可控供电成本",
                "综合线损率",
                "终端数据采集完整率（等于采集完整率）",
                "欠费数据");

        //初始化测试指标别名
        menuItem = Arrays.asList("form-entry",
                "informatization",
                "satisfaction",
                "danger-rectification",
                "equipment-defect",
                "rural-voltage",
                "outage-time",
                "urban-residents",
                "recovery-electric",
                "loss-rate",
                "business-income",
                "controllable-cost",
                "integrated-loss",
                "data-integrity",
                "lack-rate");

    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        for (int i = 0; i < menuItem.size(); i++) {
            Map<String, String> menuMap = new HashMap<>();
            menuMap.put("id", menuId.get(i));
            menuMap.put("name", menuName.get(i));
            when(getWebConnDao.findIndexNameByCheckMenu(menuItem.get(i))).thenReturn(menuMap);
        }
        for (int i = 0; i < dataMonth.size(); i++) {
            for (int k = 0; k < 15; k++) {

                //初始化某个时间下乌当指标测试数据
                wudangMap.put("id", menuId.get(k));
                wudangMap.put("fx", direction.get(new Random().nextInt(2)));
                wudangMap.put("isImportent", importent.get(new Random().nextInt(3)));
                wudangMap.put("time", dataMonth.get(i));
                wudangMap.put("indexName", menuName.get(k));
                wudangMap.put("completValue", completValue.get(new Random().nextInt(15)));
                if (!"1015".equals(wudangMap.get("id"))) {
                    wudangMap.put("targetValue", targetValue.get(new Random().nextInt(14)));
                } else {
                    wudangMap.put("targetValue", "0");
                }

                if (Double.parseDouble(wudangMap.get("completValue")) - Double.parseDouble(wudangMap.get("targetValue")) >= 0) {
                    wudangMap.put("isGetTarget", "1");
                } else {
                    wudangMap.put("isGetTarget", "0");
                }
                wudangMap.put("orgCode", "060101");
                datalistOfYear.add(wudangMap);
                if (i == 0) {
                    datalistOfMonth.add(wudangMap);
                }
            }
        }
    }

    @Test
    public void insert() throws JsonProcessingException {
        //断言指标名称
        Map<String, String> menuMap = new HashMap<>();
        menuMap.put("id", "1015");
        menuMap.put("name", "欠费数据");

        assertThat(getWebConnDao.findIndexNameByCheckMenu("lack-rate"), equalTo(menuMap));

        //获取重复信息，第一次执行返回0，第二次执行返回1
        when(getWebConnDao.duplicateData("1001", "201806", "060101", "quota")).thenReturn(0).thenReturn(1);
        //断言第一次获取，期望值是0
        assertEquals(0, getWebConnDao.duplicateData("1001", "201806", "060101", "quota"));
        //执行新增操作，预期成功返回1
        when(getWebConnDao.addIndexInfo(isA(Indexes.class))).thenReturn(1);
        //断言新增操作，预期值是1
        assertEquals(1, getWebConnDao.addIndexInfo(Mockito.mock(Indexes.class)));
        //断言第二次获取，预期返回1
        assertEquals(1, getWebConnDao.duplicateData("1001", "201806", "060101", "quota"));
        //执行更新操作,预期成功返回1
        when(getWebConnDao.updateQuotaData(isA(Indexes.class))).thenReturn(1);
        //断言更新操作，预期返回1
        assertEquals(1, getWebConnDao.updateQuotaData(Mockito.mock(Indexes.class)));
        //断言整个方法，预期返回Json字符串
        String str = webController.insert(data);
        assertEquals(JsonUtil.objToJson(Result.success("", "成功")), str);
    }

    @Test
    public void getUserCompany() throws JsonProcessingException {
        //传入参数为null,预期返回null
        when(getWebConnDao.findOrgNameByOrgCode(null)).thenReturn(null);
        //断言预期null,实际返回null
        assertEquals(null, getWebConnDao.findOrgNameByOrgCode(null));
        //传入参数Null以外的字符串，返回List<>;
        when(getWebConnDao.findOrgNameByOrgCode("060101")).thenReturn(companyList);
        assertEquals(companyList, getWebConnDao.findOrgNameByOrgCode("060101"));
        assertThat(webController.getUserCompany(), equalTo(JsonUtil.objToJson(Result.success(companyList, "查询成功"))));
    }

    @Test
    public void saveWeights() throws JsonProcessingException {
        //断言指标名称
        Map<String, String> menuMap = new HashMap<>();
        menuMap.put("id", "1015");
        menuMap.put("name", "欠费数据");
        assertThat(getWebConnDao.findIndexNameByCheckMenu("lack-rate"), equalTo(menuMap));

        when(getWebConnDao.duplicateData("1015", "2018-06", "", "weights")).thenReturn(0).thenReturn(1);

        when(getWebConnDao.addIndexWeights(isA(QuotaWeights.class))).thenReturn(1);
        assertEquals(1, getWebConnDao.addIndexWeights(new QuotaWeights()));

        when(getWebConnDao.updateWeightsData(isA(QuotaWeights.class))).thenReturn(1);
        assertEquals(1, getWebConnDao.updateWeightsData(new QuotaWeights()));

        assertThat(webController.saveWeights(weights), equalTo(JsonUtil.objToJson(Result.success("", "成功"))));
    }

    @Test
    public void showWebInfo() throws JsonProcessingException {
        when(comm.canFormat("2018-06")).thenReturn(true);
        when(comm.canFormat("2018")).thenReturn(true);
        when(comm.canFormat("null")).thenReturn(false);
        if (comm.canFormat("2018")) {
            List<Map<String, Object>> data = new ArrayList<>();
            when(getWebConnDao.findMonthOfYear("2018", "", "")).thenReturn(allDate);
            for (String s : allDate) {
                Map<String, Object> map = webController.allInfoMap(s);
                Map<String, String> indexmap = webController.makeIndexManagement(s);
                map.put("indexManagement", indexmap);
                data.add(map);
            }
            String str = webController.getWebShowInfo("2018");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        if (comm.canFormat("2018-06")) {
            List<Map<String, Object>> data = new ArrayList<>();
            String queryMonth = "2018-06".replace("-", "");
            Map<String, Object> map = webController.allInfoMap(queryMonth);
            Map<String, String> indexmap = webController.makeIndexManagement(queryMonth);
            String str = webController.getWebShowInfo("2018-06");
            map.put("indexManagement", indexmap);
            data.add(map);
            assertEquals(str, JsonUtil.objToJson(Result.success(data, "查询成功")));
        }
        if (comm.canFormat("null") == false) {
            List<Map<String, Object>> data = new ArrayList<>();
            when(getWebConnDao.findAllMonth("")).thenReturn(allDate);
            for (String y : allDate) {
                Map<String, Object> map = webController.allInfoMap(y);
                Map<String, String> indexmap = webController.makeIndexManagement(y);
                map.put("indexManagement", indexmap);
                data.add(map);
            }
            String str = webController.getWebShowInfo("null");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
    }

    @Test
    public void allInfoMap() {
        List<Map<String, String>> listmap = new ArrayList<>();
        Map<String, String> map0 = new HashMap<>();
        map0.put("id", "1001");
        map0.put("fx", "1");
        map0.put("isImportent", "0");
        map0.put("time", "2018-06");
        map0.put("indexName", "百万工时工伤意外率");
        map0.put("completeValue", "91");
        map0.put("targetValue", "90");
        map0.put("orgCode", "060101");
        map0.put("isGetTarget", "1");
        listmap.add(map0);

        Map<String, String> map1 = new HashMap<>();
        map1.put("id", "1015");
        map1.put("fx", "1");
        map1.put("isImportent", "2");
        map1.put("time", "2018-06");
        map1.put("indexName", "欠费数据");
        map1.put("completeValue", "91");
        map1.put("targetValue", "90");
        map1.put("orgCode", "060101");
        map1.put("isGetTarget", "1");
        listmap.add(map1);

        Map<String, String> map2 = new HashMap<>();
        map2.put("id", "1010");
        map2.put("fx", "1");
        map2.put("isImportent", "1");
        map2.put("time", "2018-06");
        map2.put("indexName", "10kV有损线损率");
        map2.put("completeValue", "91");
        map2.put("targetValue", "90");
        map2.put("orgCode", "060101");
        map2.put("isGetTarget", "1");
        listmap.add(map2);


        if (listmap.isEmpty() == false) {
            when(getWebConnDao.findAllInfoByMonth("201806", "060101")).thenReturn(listmap);
            when(comm.getRanked("201806", "1001", 91D, "06", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "0601", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "060101", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1010", 91D, "0601", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1010", 91D, "060101", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1010", 91D, "06", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1015", 91D, "0601", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1015", 91D, "06", "1", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1015", 91D, "060101", "1", "6")).thenReturn("1");
            when(comm.getYBValue("201806", "1001", "060101", null)).thenReturn("1");
            when(comm.getYBValue("201806", "1010", "060101", null)).thenReturn("1");
            when(comm.getYBValue("201806", "1015", "060101", null)).thenReturn("1");
            when(comm.getRRValue("201806", "1001", "060101", null)).thenReturn("1");
            when(comm.getRRValue("201806", "1010", "060101", null)).thenReturn("1");
            when(comm.getRRValue("201806", "1015", "060101", null)).thenReturn("1");

            Map<String, Object> datalist = new HashMap<>(16);
            List<Map<String, String>> particulars = new ArrayList<>();
            Map<String, Object> lineLoss = new HashMap<>(16);
            Map<String, Object> costData = new HashMap<>(16);

            for (Map<String, String> map : listmap) {
                map.put("provinceRanked", "1");
                map.put("countyRanked", "1");
                map.put("stationRanked", "1");
                map.put("yearBasis", "1");
                map.put("relativeRatio", "1");
                particulars.add(map);
                if ("1".equals(String.valueOf(map.get("isImportent")))) {
                    lineLoss.put("id", map.get("id"));
                    lineLoss.put("provinceRanked", "1");
                    lineLoss.put("completeValue", map.get("completeValue"));
                }
                if ("2".equals(String.valueOf(map.get("isImportent")))) {
                    costData.put("id", map.get("id"));
                    costData.put("completeValue", map.get("completeValue"));
                }
            }
            datalist.put("lineLoss", lineLoss);
            datalist.put("lackData", costData);
            datalist.put("particulars", particulars);

            Map<String, Object> w = webController.allInfoMap("201806");

            assertEquals(datalist, w);
        }
    }

    @Test
    public void makeIndexManagement() {
        when(getWebConnDao.makeIndexManagement(argThat(new ArgumentMatcher<String>() {
            @Override
            public boolean matches(Object o) {
                return allInfoMonth.contains(o.toString());
            }
        }), anyString())).thenReturn(anyList());
        assertEquals(anyList(), getWebConnDao.makeIndexManagement("201802", "060101"));
    }

    @Test
    public void getSpecificInfo() throws JsonProcessingException {
        List<Map<String, String>> listmap = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("date", "2018-06");
        map.put("code", "060101");
        map.put("indexName", "百万工时工伤意外率");
        map.put("completeValue", "91");
        map.put("targetValue", "90");
        map.put("fx", "0");
        map.put("isImportent", "0");
        listmap.add(map);
        when(comm.canFormat("2018-06")).thenReturn(true);
        when(comm.canFormat("2018")).thenReturn(true);
        when(comm.canFormat("null")).thenReturn(false);
        if (comm.canFormat("2018")) {
            List<Map<String, Object>> data = new ArrayList();
            Map<String, Object> datas;
            when(getWebConnDao.findMonthOfYear("2018", "", "")).thenReturn(allDate);
            for (String month : allDate) {
                datas = webController.allDetail(month, "1001");
                data.add(datas);
            }
            String str = webController.getSpecificInfo("2018", "1001");
            assertEquals(JsonUtil.objToJson(Result.success(data, "查询成功")), str);
        }
        if (comm.canFormat("2018-06")) {
            List<Map<String, Object>> data = new ArrayList();
            Map<String, Object> datas;
            String queryMonth = "2018-06".replace("-", "");
            datas = webController.allDetail(queryMonth, "1001");
            if (datas.isEmpty() == false) {
                data.add(datas);
            }
            String str = webController.getSpecificInfo("2018-06", "1001");
            assertEquals(JsonUtil.objToJson(Result.success(data, "查询成功")), str);
        }
        if (comm.canFormat("null") == false) {
            List<Map<String, Object>> data = new ArrayList();
            Map<String, Object> datas;
            String queryMonth = "";
            when(getWebConnDao.findAllMonth("")).thenReturn(allDate);
            for (String s : allDate) {
                when(getWebConnDao.findIndexInfoByMonthAndId(s, "1001")).thenReturn(listmap);
                if (listmap.isEmpty() == true) {
                    continue;
                } else {
                    queryMonth = s;
                    break;
                }
            }
            datas = webController.allDetail(queryMonth, "1001");
            data.add(datas);

            String str = webController.getSpecificInfo("null", "1001");
            assertEquals(JsonUtil.objToJson(Result.success(data, "查询成功")), str);
        }
    }

    @Test
    public void getLineLoss() throws JsonProcessingException {
        when(getWebConnDao.queryIndexIdByIsImportent(1)).thenReturn("1010");
        when(comm.canFormat("2018-06")).thenReturn(true);
        when(comm.canFormat("2018")).thenReturn(true);
        when(comm.canFormat("null")).thenReturn(false);
        if (comm.canFormat("2018")) {
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            when(getWebConnDao.findMonthOfYear("2018", "1010", "")).thenReturn(allDate);
            for (String month : allDate) {
                datas = webController.allInfoMapOfDetail(month, "060101", "1010");
                if (datas.isEmpty() == false) {
                    data.add(datas);
                }
            }
            String str = webController.getLineLoss("2018", "1010");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        if (comm.canFormat("2018-06")) {
            String queryMonth = "2018-06".replace("-", "");
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            datas = webController.allInfoMapOfDetail(queryMonth, "060101", "1010");
            data.add(datas);
            String str = webController.getLineLoss("2018-06", "1010");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        Map<String, String> map = new HashMap<>();
        map.put("date", "201806");
        map.put("code", "060101");
        map.put("indexName", "10千伏以下有损线损率");
        map.put("completeValue", "92");
        map.put("targetValue", "90");
        map.put("fx", "0");
        map.put("isImportent", "1");
        List<Map<String, String>> listmap = new ArrayList<>();
        listmap.add(map);

        if (comm.canFormat("null") == false) {
            String queryMonth = "2018-06".replace("-", "");
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            when(getWebConnDao.findAllMonth("")).thenReturn(allDate);
            for (String s : allDate) {
                when(getWebConnDao.findIndexInfoByMonthAndId(s, "1010")).thenReturn(listmap);
                if (listmap.isEmpty() == true) {
                    continue;
                } else {
                    queryMonth = s;
                    break;
                }
            }
            datas = webController.allInfoMapOfDetail(queryMonth, "060101", "1010");
            data.add(datas);
        }
    }

    @Test
    public void getLackData() throws JsonProcessingException {
        when(getWebConnDao.queryIndexIdByIsImportent(2)).thenReturn("1015");
        when(comm.canFormat("2018-06")).thenReturn(true);
        when(comm.canFormat("2018")).thenReturn(true);
        when(comm.canFormat("null")).thenReturn(false);
        if (comm.canFormat("2018")) {
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            when(getWebConnDao.findMonthOfYear("2018", "1015", "")).thenReturn(allDate);
            for (String month : allDate) {
                datas = webController.allInfoMapOfDetail(month, "060101", "1015");
                if (datas.isEmpty() == false) {
                    data.add(datas);
                }
            }
            String str = webController.getLackData("2018", "1015");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        if (comm.canFormat("2018-06")) {
            String queryMonth = "2018-06".replace("-", "");
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            datas = webController.allInfoMapOfDetail(queryMonth, "060101", "1015");
            data.add(datas);
            String str = webController.getLackData("2018-06", "1015");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        Map<String, String> map = new HashMap<>();
        map.put("date", "201806");
        map.put("code", "060101");
        map.put("indexName", "欠费数据");
        map.put("completeValue", "92");
        map.put("targetValue", "90");
        map.put("fx", "0");
        map.put("isImportent", "1");
        List<Map<String, String>> listmap = new ArrayList<>();
        listmap.add(map);

        if (comm.canFormat("null") == false) {
            String queryMonth = "";
            List<Map<String, Object>> data = new ArrayList<>();
            Map<String, Object> datas;
            when(getWebConnDao.findAllMonth("")).thenReturn(allDate);
            for (String s : allDate) {
                when(getWebConnDao.findIndexInfoByMonthAndId(s, "1015")).thenReturn(listmap);
                if (listmap.isEmpty() == true) {
                    continue;
                } else {
                    queryMonth = s;
                    break;
                }
            }
            data = webController.getImportentData("201806", "060101", "1015");
            String str = webController.getLackData("null", "1015");
            assertEquals(JsonUtil.objToJson(Result.success(data, "查询成功")), str);
        }
    }

    @Test
    public void getMenuList() throws JsonProcessingException {
        List<Map<String, Object>> listmap = new ArrayList<>();
        Map<String, Object> map0 = new HashMap<>();
        map0.put("positionName", "乌当供电局");
        map0.put("positionId", "060101");
        map0.put("positionPid", "0601");
        Map<String, Object> map1 = new HashMap<>();
        map1.put("positionName", "下坝供电所");
        map1.put("positionId", "06010107");
        map1.put("positionPid", "060101");
        listmap.add(map0);
        listmap.add(map1);

        when(getWebConnDao.findOrgInfo("060101", "8")).thenReturn(listmap);
        List<Map<String, Object>> dataList = new ArrayList<>();
        List<Map<String, Object>> childreno = new ArrayList<>();
        Map<String, Object> gdj = new HashMap<>(16);
        for (Map<String, Object> map : listmap) {
            if (map.get("positionId").toString().length() == 6) {
                gdj.putAll(map);
                gdj.put("positionLevel", "XJ");
            } else {
                map.put("positionLevel", "GDS");
                List<Map<String, Object>> children = new ArrayList<>();
                List<Map<String, Object>> tqlist = new ArrayList<>();
                Map<String, Object> map2 = new HashMap<>();
                map2.put("positionName", "40号公变下坝谷定变");
                map2.put("positionId", "2077276");
                map2.put("positionPid", "060107");
                Map<String, Object> map3 = new HashMap<>();
                map3.put("positionName", "7号公变下坝小坝变");
                map3.put("positionId", "2077277");
                map3.put("positionPid", "06010107");
                tqlist.add(map2);
                tqlist.add(map3);
                when(getWebConnDao.findTQInfo(map.get("positionId").toString())).thenReturn(tqlist);
                for (Map<String, Object> tq : tqlist) {
                    tq.put("positionLevel", "TQ");
                    tq.put("children", new ArrayList<>());
                    children.add(tq);
                }
                map.put("children", children);
                childreno.add(map);
            }
        }
        gdj.put("children", childreno);
        dataList.add(gdj);
        String str = webController.getMenuList();
        assertThat(str, equalTo(JsonUtil.objToJson(Result.success(dataList, "查询成功"))));
    }

    @Test
    public void getInfoByDateType() throws JsonProcessingException {
        String queryMonth = "2018-06".replace("-", "");
        when(comm.canFormat("2018-06")).thenReturn(true);
        when(comm.canFormat("2018")).thenReturn(true);
        when(comm.canFormat("null")).thenReturn(false);
        if (comm.canFormat("2018-06")) {
            List<Map<String, Map<String, String>>> data = new ArrayList<>();
            when(getWebConnDao.findMonthOfYear(queryMonth.substring(0, 4), null, "")).thenReturn(allDate);
            for (String month : allDate) {
                Map<String, Map<String, String>> dataList = webController.dataMap(month);
                data.add(dataList);
            }
            String str = webController.getInfoByDateType("2018-06", "year");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        if (comm.canFormat("2018-06")) {
            List<Map<String, Map<String, String>>> data = new ArrayList<>();
            Map<String, Map<String, String>> dataList = webController.dataMap(queryMonth);
            data.add(dataList);
            String str = webController.getInfoByDateType("2018-06", "");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
        if (comm.canFormat("null") == false) {
            List<Map<String, Map<String, String>>> data = new ArrayList<>();
            when(getWebConnDao.findAllMonth("")).thenReturn(allDate);
            for (String month : allDate) {
                Map<String, Map<String, String>> dataList = webController.dataMap(month);
                data.add(dataList);
            }
            String str = webController.getInfoByDateType(null, "");
            assertThat(str, equalTo(JsonUtil.objToJson(Result.success(data, "查询成功"))));
        }
    }

    @Test
    public void allDetail() {
        List<Map<String, String>> listmap = new ArrayList<>();
        Map<String, String> map0 = new HashMap<>();
        map0.put("id", "1001");
        map0.put("fx", "1");
        map0.put("isImportent", "0");
        map0.put("time", "2018-06");
        map0.put("indexName", "百万工时工伤意外率");
        map0.put("completeValue", "91");
        map0.put("targetValue", "90");
        map0.put("orgCode", "060101");
        map0.put("isGetTarget", "1");
        listmap.add(map0);

        List<Map<String, String>> infolist = new ArrayList<>();
        Map<String, String> indexinfo = new HashMap<>();
        indexinfo.put("date", "2018-06");
        indexinfo.put("code", "060101");
        indexinfo.put("indexName", "百万工时工伤意外率");
        indexinfo.put("completeValue", "91");
        indexinfo.put("targetValue", "90");
        indexinfo.put("fx", "0");
        indexinfo.put("isImportent", "0");
        infolist.add(indexinfo);


        List<Map<String, String>> accomplishComp = new ArrayList<>();
        Map<String, String> comp = new HashMap<>();
        comp.put("id", "1001");
        comp.put("monad", "下坝供电所");
        comp.put("completeValue", "91");
        comp.put("targetValue", "90");
        comp.put("code", "06010107");
        comp.put("fx", "0");
        accomplishComp.add(comp);
        when(getWebConnDao.findAllInfoByMonth("201806", "060101")).thenReturn(listmap);
        when(getWebConnDao.findIndexInfoByMonthAndId("201806", "1001")).thenReturn(infolist);
        Map<String, String> map = infolist.get(0);
        if (indexinfo.isEmpty() == false) {
            Map<String, Object> data = new HashMap<>(16);
            List<Map<String, String>> particulars = new ArrayList<>();
            Map<String, Object> lineLoss = new HashMap<>(16);
            Map<String, Object> costData = new HashMap<>(16);
            when(comm.getRanked("201806", "1001", 91D, "06", "0", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "0601", "0", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "060101", "0", "6")).thenReturn("1");
            when(comm.getYBValue("201806", "1001", "060101", null)).thenReturn("1");
            when(comm.getRRValue("201806", "1001", "060101", null)).thenReturn("1");
            map.put("provinceRanked", "1");
            map.put("countyRanked", "1");
            map.put("stationRanked", "1");
            map.put("yearBasis", "1");
            map.put("relativeRatio", "1");
            if ("1".equals(String.valueOf(map.get("isImportent")))) {
                lineLoss.put("id", map.get("id"));
                lineLoss.put("provinceRanked", "1");
                lineLoss.put("completeValue", map.get("completeValue"));
            }
            if ("2".equals(String.valueOf(map.get("isImportent")))) {
                costData.put("id", map.get("id"));
                costData.put("completeValue", map.get("completeValue"));
            }
            List<String> monts = new ArrayList<>();
            monts.add("201806");

            when(getWebConnDao.findMonthOfYear("2018", null, "201806")).thenReturn(monts);

            for (String s : monts) {
                for (Map<String, String> mapone : infolist) {
                    mapone.put("provinceRanked", "1");
                    mapone.put("countyRanked", "1");
                    mapone.put("stationRanked", "1");
                    mapone.put("yearBasis", "1");
                    mapone.put("relativeRatio", "1");
                    particulars.add(mapone);
                }
            }
            Map<String, String> mounthAndBasis = webController.mounthAndRY("201806", "91", "90", "1");
            Map<String, String> mounthAndRatio = webController.mounthAndRY("201806", "91", "90", "");
            data.put("accomplishComp", new HashMap<>());
            data.put("particulars", particulars);
            data.put("mounthAndBasis", mounthAndBasis);
            data.put("mounthAndRatio", mounthAndRatio);
            data.put("currentData", map);
            Map<String, Object> m = webController.allDetail("201806", "1001");
            assertEquals(data, m);
        }

    }


    @Test
    public void dataMap() {
        List<Map<String, String>> listmap = new ArrayList<>();
        Map<String, String> map0 = new HashMap<>();
        map0.put("id", "1001");
        map0.put("fx", "1");
        map0.put("isImportent", "0");
        map0.put("time", "2018-06");
        map0.put("indexName", "百万工时工伤意外率");
        map0.put("completeValue", "91");
        map0.put("targetValue", "90");
        map0.put("orgCode", "060101");
        map0.put("isGetTarget", "1");
        listmap.add(map0);

        Map<String, String> map1 = new HashMap<>();
        map1.put("id", "1015");
        map1.put("fx", "1");
        map1.put("isImportent", "2");
        map1.put("time", "2018-06");
        map1.put("indexName", "欠费数据");
        map1.put("completeValue", "91");
        map1.put("targetValue", "90");
        map1.put("orgCode", "060101");
        map1.put("isGetTarget", "1");
        listmap.add(map1);

        Map<String, String> map2 = new HashMap<>();
        map2.put("id", "1010");
        map2.put("fx", "1");
        map2.put("isImportent", "1");
        map2.put("time", "2018-06");
        map2.put("indexName", "10kV有损线损率");
        map2.put("completeValue", "91");
        map2.put("targetValue", "90");
        map2.put("orgCode", "060101");
        map2.put("isGetTarget", "1");
        listmap.add(map2);
        Map<String, Map<String, String>> data = new HashMap<>(16);
        when(getWebConnDao.findAllInfoByMonth("201806", "060101")).thenReturn(listmap);
        Map<String, String> lineLoss = new HashMap<>(16);
        Map<String, String> costData = new HashMap<>(16);
        for (Map<String, String> map : listmap) {
            if ("1".equals(map.get("isImportent"))) {
                when(comm.getRanked("201806", "1010", Double.parseDouble(map.get("completeValue")), "06", "0", "6")).thenReturn("1");
                lineLoss.put("id", map.get("id"));
                lineLoss.put("provinceRanked", "1");
                lineLoss.put("completeValue", map.get("completeValue"));
            }
            if ("2".equals(map.get("isImportent"))) {
                costData.put("id", map.get("id"));
                costData.put("completeValue", map.get("completeValue"));
            }
        }
        List<Map<String, String>> maps = new ArrayList<>();
        Map<String, String> map3 = new HashMap<>();
        map3.put("id", "1001");
        map3.put("completeValue", "91");
        map3.put("orgCode", "060101");
        map3.put("isGetTarget", "1");
        maps.add(map3);
        Map<String, String> map4 = new HashMap<>();
        map4.put("id", "1001");
        map4.put("completeValue", "91");
        map4.put("orgCode", "060101");
        map4.put("isGetTarget", "1");
        maps.add(map4);
        Map<String, String> map5 = new HashMap<>();
        map5.put("id", "1001");
        map5.put("completeValue", "91");
        map5.put("orgCode", "060101");
        map5.put("isGetTarget", "1");
        maps.add(map5);

        when(getWebConnDao.findAllCount("201806", "060101", "6", "")).thenReturn("91");
        when(getWebConnDao.findAllCount("201806", "060101", "6", "91")).thenReturn("1");
        when(getWebConnDao.makeIndexManagement("201806", "060101")).thenReturn(maps);
        when(comm.getSynthesisRanked("2018-06", "060101")).thenReturn("1");
        String n = comm.getSynthesisRanked("2018-06", "060101");
        System.out.println(n);
        Map<String, String> indexManagement = new HashMap<>(16);
        indexManagement.put("Incomplete", "0");
        indexManagement.put("accomplish", "15");
        indexManagement.put("date", "2018-06");
        indexManagement.put("provinceRanked", n);
        data.put("indexManagement", indexManagement);
        data.put("lineLoss", lineLoss);
        data.put("costData", costData);
        Map<String, Map<String, String>> d = webController.dataMap("201806");
        assertThat(data, equalTo(d));
    }


    @Test
    public void allInfoMapOfDetail() {
        List<Map<String, String>> listmap = new ArrayList<>();
        Map<String, String> map0 = new HashMap<>();
        map0.put("id", "1001");
        map0.put("fx", "1");
        map0.put("isImportent", "0");
        map0.put("time", "2018-06");
        map0.put("indexName", "百万工时工伤意外率");
        map0.put("completeValue", "91");
        map0.put("targetValue", "90");
        map0.put("orgCode", "060101");
        map0.put("isGetTarget", "1");
        listmap.add(map0);

        Map<String, String> map1 = new HashMap<>();
        map1.put("id", "1015");
        map1.put("fx", "1");
        map1.put("isImportent", "2");
        map1.put("time", "2018-06");
        map1.put("indexName", "欠费数据");
        map1.put("completeValue", "91");
        map1.put("targetValue", "90");
        map1.put("orgCode", "060101");
        map1.put("isGetTarget", "1");
        listmap.add(map1);

        Map<String, String> map2 = new HashMap<>();
        map2.put("id", "1010");
        map2.put("fx", "1");
        map2.put("isImportent", "1");
        map2.put("time", "2018-06");
        map2.put("indexName", "10kV有损线损率");
        map2.put("completeValue", "91");
        map2.put("targetValue", "90");
        map2.put("orgCode", "060101");
        map2.put("isGetTarget", "1");
        listmap.add(map2);

        List<Map<String, String>> infolist = new ArrayList<>();
        Map<String, String> indexinfo = new HashMap<>();
        indexinfo.put("date", "2018-06");
        indexinfo.put("code", "060101");
        indexinfo.put("indexName", "isImportent");
        indexinfo.put("completeValue", "91");
        indexinfo.put("targetValue", "90");
        indexinfo.put("fx", "0");
        indexinfo.put("isImportent", "0");
        infolist.add(indexinfo);


        List<Map<String, String>> accomplishComp = new ArrayList<>();
        Map<String, String> comp = new HashMap<>();
        comp.put("id", "1001");
        comp.put("monad", "下坝供电所");
        comp.put("completeValue", "91");
        comp.put("targetValue", "90");
        comp.put("code", "06010107");
        comp.put("fx", "0");
        accomplishComp.add(comp);
        Map<String, Object> data = new HashMap<>(16);
        when(getWebConnDao.findIndexInfoByMonthAndId("201806", "1001")).thenReturn(infolist);
        if (indexinfo.isEmpty() == false) {
            when(getWebConnDao.findInfoByDateAndOrgCode("201806", "060101", "1001")).thenReturn(accomplishComp);
            when(comm.getRanked("201806", "1001", 91D, "06", "0", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "0601", "0", "6")).thenReturn("1");
            when(comm.getRanked("201806", "1001", 91D, "060101", "0", "6")).thenReturn("1");
            when(comm.getYBValue("201806", "1001", "060101", null)).thenReturn("1");
            when(comm.getRRValue("201806", "1001", "060101", null)).thenReturn("1");

            comp.put("provinceRanked", "1");
            comp.put("countyRanked", "1");
            comp.put("stationRanked", "1");
            comp.put("yearBasis", "1");
            comp.put("relativeRatio", "1");

            comp.remove("fx");
            for (Map<String, String> comp1a : accomplishComp) {
                when(comm.getRanked("201806", "1001", 91D, "06", "1", "6")).thenReturn("1");
                when(comm.getRanked("201806", "1001", 91D, "0601", "1", "6")).thenReturn("1");
                when(comm.getRanked("201806", "1001", 91D, "060101", "1", "6")).thenReturn("1");
                when(comm.getYBValue("201806", "1001", "060101", null)).thenReturn("1");
                when(comm.getRRValue("201806", "1001", "060101", null)).thenReturn("1");
                comp1a.put("provinceRanked", "1");
                comp1a.put("countyRanked", "1");
                comp1a.put("stationRanked", "1");
                comp1a.put("yearBasis", "1");
                comp1a.put("relativeRatio", "1");
            }
            data.put("currentData", comp);
            data.put("accomplishComp", accomplishComp);

        }
    }


}
