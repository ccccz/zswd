package com.dingxin.wudang.board.controller;


import com.dingxin.wudang.board.common.CommonMethod;
import com.dingxin.wudang.board.dao.GetMobInfoDao;
import com.dingxin.wudang.board.utils.JsonUtil;
import com.dingxin.wudang.common.dto.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MobControllerTest {
    @InjectMocks
    MobController mobController;

    @Mock
    GetMobInfoDao getMobInfoDao;

    @Mock
    CommonMethod commonMethod;

    private static List<String> alldate = Arrays.asList("201801");//获取到的年月
    private static List<Map<String, Object>> dataAll = new ArrayList<>();//getMobInfo最终数据
    private static List<Map<String, Double>> qfdataOfyear = new ArrayList<>();//欠费年数据
    private static List<Map<String, Double>> xsdataOfyear = new ArrayList<>();//10KV年数据
    private static List<Map<String, Object>> showMobData = new ArrayList<>();//查询到的getMobInfo接口展示数据

    @BeforeClass
    public static void initGetMobInfoExanple() {
        /**
         * 填充valueOfyear
         */
        Map<String, Double> qf = new HashMap<>();
        qf.put("complete", 187440.38);
        qf.put("target", null);
        qfdataOfyear.add(qf);

        Map<String, Double> xs = new HashMap<>();
        xs.put("complete", 5.76);
        xs.put("target", 3.0);
        xsdataOfyear.add(xs);

        /**
         * 填充显示查询showMobData
         */
        Map<String, Object> xsmobData = new HashMap<>();
        xsmobData.put("indexName", "10千伏及以下有损线损率");
        xsmobData.put("id", "10KVJYXYSXSL");
        xsmobData.put("completeValue", 5.76);
        xsmobData.put("indexTime", 201801);
        xsmobData.put("orgCode", "060101");
        xsmobData.put("isInportent", 1);
        xsmobData.put("sign", "%");
        xsmobData.put("sort", 1);
        xsmobData.put("fx", 0);
        xsmobData.put("isGetTarget", 2);
        xsmobData.put("isJoinTarget", 1);
        xsmobData.put("target_value", 3);
        xsmobData.put("targetValue", 3);

        Map<String, Object> qfmobData = new HashMap<>();
        qfmobData.put("indexName", "欠费数据");
        qfmobData.put("id", "QFBNLJ");
        qfmobData.put("completeValue", 187440.38);
        qfmobData.put("indexTime", 201801);
        qfmobData.put("orgCode", "060101");
        qfmobData.put("isInportent", "2");
        qfmobData.put("sign", "元");
        qfmobData.put("sort", 2);
        qfmobData.put("fx", 0);
        qfmobData.put("isGetTarget", 0);
        qfmobData.put("isJoinTarget", 1);
        qfmobData.put("target_value", null);
        qfmobData.put("targetValue", "-");

        showMobData.add(xsmobData);
        showMobData.add(qfmobData);

        Map<String, Object> datalist1 = new HashMap<>();//展示数据集合1
        Map<String, Object> datalist2 = new HashMap<>();//展示数据集合2
        Map<String, Object> indexManagement = new HashMap<>();
        Map<String, Object> lineLoss = new HashMap<>();
        Map<String, Object> costData = new HashMap<>();
        List<Map<String, Object>> particulars = new ArrayList<>();
        Map<String, Object> dataMap = new HashMap<>();

        indexManagement.put("date", "201801");
        indexManagement.put("Incomplete", 1);
        indexManagement.put("provinceRankedShow", true);
        indexManagement.put("stationRankedShow", false);
        indexManagement.put("dataindex", 0);
        indexManagement.put("isNew", true);
        indexManagement.put("countyRankedShow", false);
        indexManagement.put("synthesisRanked", null);

        Map<String, Object> valueOfyear1 = new HashMap<>();
        valueOfyear1.put("complete", Arrays.asList(5.76));
        valueOfyear1.put("target", Arrays.asList(3.0));

        datalist1.put("indexName", "10千伏及以下有损线损率");
        datalist1.put("id", "10KVJYXYSXSL");
        datalist1.put("completeValue", "5.76%");
        datalist1.put("indexTime", 201801);
        datalist1.put("orgCode", "060101");
        datalist1.put("isInportent", true);
        datalist1.put("sign", "%");
        datalist1.put("sort", 1);
        datalist1.put("fx", 0);
        datalist1.put("isGetTarget", 2);
        datalist1.put("isJoinTarget", 1);
        datalist1.put("target_value", 3);
        datalist1.put("targetValue", "3%");
        datalist1.put("provinceRanked", "1");
        datalist1.put("valueOfyear", valueOfyear1);
        datalist1.put("isCast", false);
        datalist1.put("isShow", false);
        datalist1.put("countyRanked", "1");
        datalist1.put("placeData", new ArrayList<>());
        datalist1.put("stationRanked", "1");

        Map<String, Object> valueOfyear2 = new HashMap<>();
        List<Object> qftarlist = new ArrayList<>();
        qftarlist.add(null);
        valueOfyear2.put("complete", Arrays.asList(187440.38));
        valueOfyear2.put("target", qftarlist);

        datalist2.put("indexName", "欠费数据");
        datalist2.put("id", "QFBNLJ");
        datalist2.put("completeValue", "187440.38元");
        datalist2.put("indexTime", 201801);
        datalist2.put("orgCode", "060101");
        datalist2.put("isInportent", true);
        datalist2.put("sign", "元");
        datalist2.put("sort", 2);
        datalist2.put("fx", 0);
        datalist2.put("isGetTarget", 0);
        datalist2.put("isJoinTarget", 1);
        datalist2.put("target_value", null);
        datalist2.put("targetValue", "-");
        datalist2.put("provinceRanked", "-");
        datalist2.put("valueOfyear", valueOfyear2);
        datalist2.put("isCast", true);
        datalist2.put("isShow", false);
        datalist2.put("countyRanked", "-");
        datalist2.put("placeData", new ArrayList<>());
        datalist2.put("stationRanked", "-");

        particulars.add(datalist1);
        particulars.add(datalist2);

        lineLoss.put("provinceRanked", "1");
        lineLoss.put("accomplish", "5.76");
        lineLoss.put("targetValue", "3");
        lineLoss.put("id", "10KVJYXYSXSL");
        lineLoss.put("isGetTarget", true);
        lineLoss.put("valueOfyear", valueOfyear1);

        costData.put("provinceRanked", "-");
        costData.put("accomplish", "187440.38");
        costData.put("targetValue", "-");
        costData.put("id", "QFBNLJ");
        costData.put("isGetTarget", false);
        costData.put("valueOfyear", valueOfyear2);

        dataMap.put("lineLoss", lineLoss);
        dataMap.put("orgCode", "060101");
        dataMap.put("indexManagement", indexManagement);
        dataMap.put("particulars", particulars);
        dataMap.put("costData", costData);

        dataAll.add(dataMap);
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getMobShowInfo() throws JsonProcessingException {

       /* when(getMobInfoDao.queryDateOfYear()).thenReturn(alldate);
        when(getMobInfoDao.showMobInfo("201801")).thenReturn(showMobData);
        when(getMobInfoDao.crValueOfYear("2018", "10KVJYXYSXSL", "060101")).thenReturn(xsdataOfyear);
        when(getMobInfoDao.crValueOfYear("2018", "QFBNLJ", "060101")).thenReturn(qfdataOfyear);*/
        when(commonMethod.getRanked("201801", "10KVJYXYSXSL", 5.76, "060101", "0", "6")).thenReturn("1");
        when(commonMethod.getRanked("201801", "QFBNLJ", 187440.38, "060101", "0", "6")).thenReturn("-");
        when(commonMethod.getRanked("201801", "10KVJYXYSXSL", 5.76, "0601", "0", "6")).thenReturn("1");
        when(commonMethod.getRanked("201801", "QFBNLJ", 187440.38, "0601", "0", "6")).thenReturn("-");
        when(commonMethod.getRanked("201801", "10KVJYXYSXSL", 5.76, "06", "0", "6")).thenReturn("1");
        when(commonMethod.getRanked("201801", "QFBNLJ", 187440.38, "06", "0", "6")).thenReturn("-");

       /* assertThat(mobController.getMobShowInfo(), equalTo(JsonUtil.objToJson(Result.success(dataAll, "查询成功"))));*/
    }
}
