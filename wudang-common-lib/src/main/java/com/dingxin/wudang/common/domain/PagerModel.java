package com.dingxin.wudang.common.domain;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * 分页模型，也是所有实体类的基类
 *
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
public class PagerModel extends BaseModel{
	/**
	 * 总数
	 */
	private int total;
	/**
	 * 分页集合列表
	 */
	private List list = new ArrayList();
	/**
	 *  总页数
	 */
	private int pagerSize;


    /**
     * Get pager size.
     *
     * @return the int
     */
    public int getPagerSize() {
		return pagerSize;
	}

    /**
     * Set pager size.
     *
     * @param pagerSize the pager size
     */
    public void setPagerSize(int pagerSize) {
		this.pagerSize = pagerSize;
	}

    /**
     * Get total.
     *
     * @return the int
     */
    public int getTotal() {
		return total;
	}

    /**
     * Set total.
     *
     * @param total the total
     */
    public void setTotal(int total) {
		this.total = total;
	}

    /**
     * Get list list.
     *
     * @return the list
     */
    public List getList() {
		return list == null ? new LinkedList() : list;
	}

    /**
     * Set list.
     *
     * @param list the list
     */
    public void setList(List list) {
		this.list = list;
	}
}
