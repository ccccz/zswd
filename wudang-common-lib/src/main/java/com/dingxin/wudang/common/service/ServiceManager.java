package com.dingxin.wudang.common.service;


import com.dingxin.wudang.common.dao.DaoManager;
import com.dingxin.wudang.common.domain.BaseModel;
import com.dingxin.wudang.common.domain.PagerModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * @param <E>   the type parameter
 * @param <DAO> the type parameter
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
public abstract class ServiceManager<E extends BaseModel, DAO extends DaoManager<E>> implements BaseService<E> {
    /**
     * Dao
     */
    @Autowired
	protected DAO dao;

    /**
     * Get dao dao.
     *
     * @return the dao
     */
    public DAO getDao() {
		return dao;
	}

    /**
     * Set dao.
     *
     * @param dao the dao
     */
    public abstract void setDao(DAO dao);

	/**
	 * 添加
	 * 
	 * @param e
	 * @return
	 */
	public int insert(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		e.setCreatedTime(new Date());
		return dao.insert(e);
	}


	/**
	 * 删除
	 * 
	 * @param e
	 * @return
	 */
	public int delete(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.delete(e);
	}

	/**
	 * 删除
	 *
	 * @param
	 * @return
	 */
	public int deleteAll() {
		return dao.deleteAll();
	}


	/**
	 * 修改
	 * 
	 * @param e
	 * @return
	 */
	public int update(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		e.setUpdatedTime(new Date());
		return dao.update(e);
	}
	
	/**
	 * 修改一条记录
	 * 
	 * @param e
	 * @return
	 */
	public int updateByPrimaryKey(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		e.setUpdatedTime(new Date());
		return dao.updateByPrimaryKey(e);
	}

	/**
	 * 修改一条记录
	 *
	 * @param e
	 * @return
	 */
	public int updateByPrimaryKeySelective(E e){
		if(e==null){
			throw new NullPointerException();
		}
		e.setUpdatedTime(new Date());
		return dao.updateByPrimaryKeySelective(e);
	}

	/**
	 * 查询一条记录
	 * 
	 * @param e
	 * @return
	 */
	public E selectOne(E e) {
		return dao.selectOne(e);
	}

	/**
	 * 分页查询
	 * 
	 * @param e
	 * @return
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public PagerModel selectPageList(E e) {
		List list = dao.selectPageList(e);
		PagerModel pm = new PagerModel();
		pm.setList(list);
		Object oneC = dao.selectPageCount(e);
		if(oneC!=null){
			pm.setTotal(Integer.parseInt(oneC.toString()));
		}else{
			pm.setTotal(0);
		}
		return pm;
	}

	@Override
	public int selectPageCount(E e){
		return dao.selectPageCount(e);
	}

	public List<E> selectList(E e) {
		return dao.selectList(e);
	}

	@Override
	public E selectByPrimaryKey(Long id) {
		return dao.selectByPrimaryKey(id);
	}
}
