package com.dingxin.wudang.common.exception;


/**
 * Created by fengyang.zhong on 9/21/2017.
 *
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
public class ServiceException extends RuntimeException {
    /**
     * Service exception.
     *
     * @param msg the msg
     */
    public ServiceException(String msg) {
        super(msg);
    }
}