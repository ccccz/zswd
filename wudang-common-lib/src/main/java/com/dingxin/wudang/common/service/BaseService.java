package com.dingxin.wudang.common.service;


import com.dingxin.wudang.common.domain.BaseModel;
import com.dingxin.wudang.common.domain.PagerModel;

import java.util.List;


/**
 * 该接口提供业务逻辑最基本的服务，所有的业逻辑类都必须实现此接口，这样该业务逻辑类对应
 * 的action就免去了写基本selectList、insert、update、toEdit、deletes麻烦s
 *
 * @param <E> the type parameter
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
public interface BaseService<E extends BaseModel> {
    /**
     * 添加
     *
     * @param e the e
     * @return int
     */
    int insert(E e);

    /**
     * 删除
     *
     * @param e the e
     * @return int
     */
    int delete(E e);

    /**
     * 删除
     *
     * @param
     * @return int
     */
    int deleteAll();

    /**
     * 修改
     *
     * @param e the e
     * @return int
     */
    int update(E e);

    /**
     * 修改一条记录
     *
     * @param e the e
     * @return int
     */
    int updateByPrimaryKey(E e);

    /**
     * 修改一条记录
     *
     * @param e the e
     * @return int
     */
    int updateByPrimaryKeySelective(E e);

    /**
     * 查询一条记录
     *
     * @param e the e
     * @return e
     */
    E selectOne(E e);

    /**
     * 根据ID查询一条记录
     *
     * @param id the id
     * @return e
     */
    E selectByPrimaryKey(Long id);

    /**
     * 分页查询
     *
     * @param e the e
     * @return pager model
     */
    PagerModel selectPageList(E e) ;

    /**
     * 分页总数
     *
     * @param e the e
     * @return int
     */
    int selectPageCount(E e);

    /**
     * 根据条件查询所有
     *
     * @param e the e
     * @return list
     */
    List<E> selectList(E e);

}
