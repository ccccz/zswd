package com.dingxin.wudang.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
@Configuration
@ComponentScan(basePackages = "com.dingxin")
public class CommonConfiguration {
}
