package com.dingxin.wudang.common.exception;

import com.dingxin.wudang.common.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.sql.SQLException;
import java.util.Set;

/**
 * Created by fengyang.zhong on 9/21/2017.
 *
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class CommonExceptionAdvice {

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Result handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        log.error("缺少请求参数", e);
        return  Result.fail(400,"required_parameter_is_not_present");
    }

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Result handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error("参数解析失败", e);
        return  Result.fail(400,"could_not_read_json");
    }

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("参数验证失败", e);
        BindingResult result = e.getBindingResult();
        FieldError error = result.getFieldError();
        String field = error.getField();
        String code = error.getDefaultMessage();
        String message = String.format("%s:%s", field, code);
        return  Result.fail(400,message);
    }

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public Result handleBindException(BindException e) {
        log.error("参数绑定失败", e);
        BindingResult result = e.getBindingResult();
        FieldError error = result.getFieldError();
        String field = error.getField();
        String code = error.getDefaultMessage();
        String message = String.format("%s:%s", field, code);
        return  Result.fail(400,message);
    }

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Result handleServiceException(ConstraintViolationException e) {
        log.error("参数验证失败", e);
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        ConstraintViolation<?> violation = violations.iterator().next();
        String message = violation.getMessage();
        return  Result.fail(400,"parameter:" + message);
    }

    /**
     * 400 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    public Result handleValidationException(ValidationException e) {
        log.error("参数验证失败", e);
        return  Result.fail(400,"validation_exception");
    }

    /**
     * 404 - Bad Request
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public Result handleNoHandlerFoundException( NoHandlerFoundException e) {
        log.error("参数验证失败",e);
        return  Result.fail(404,"service not found");
    }

    /**
     * 405 - Method Not Allowed
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error("不支持当前请求方法", e);
        return  Result.fail(405,"request_method_not_supported");
    }

    /**
     * 415 - Unsupported Media Type
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public Result handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        log.error("不支持当前媒体类型", e);
        return  Result.fail(415,"content_type_not_supported");
    }

    /**
     * 500 - Internal Server Error
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServiceException.class)
    public Result handleServiceException(ServiceException e) {
        log.error("业务逻辑异常", e);
        return  Result.fail(500,"业务逻辑异常：" + e.getMessage());
    }

    /**
     * 500 - Internal Server Error
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        log.error("Service异常", e);
        return  Result.fail(500,"Service异常：" + e.getMessage());
    }

    /**
     * 操作数据库出现异常:名称重复，外键关联
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public Result handleException(DataIntegrityViolationException e) {
        log.error("操作数据库出现异常:", e);
        return  Result.fail(600,"操作数据库出现异常：字段重复、有外键关联等");
    }

    /**
     * SQL exception
     *
     * @param e the e
     * @return the result
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(SQLException.class)
    public Result handleSQLException(SQLException e) {
        log.error("操作数据库出现异常:", e);
        return  Result.fail(600,"操作数据库出现异常：" + e.getMessage());
    }
}
