package com.dingxin.wudang.common.dto;

/**
 * @author : chuncheng.peng
 * @date : 2018-03-12
 */
public class ResultCode  {

    /**
     * SUCCESS
     */
    public static final int SUCCESS = 200;

    /**
     * FAIL_CODE_NO_APPLICANT_FOUND
     */
    public static final int FAIL_CODE_NO_APPLICANT_FOUND = 600;

    /**
     * FAIL_CODE_VALIDATION_CODE_ERROR
     */
    public static final int FAIL_CODE_VALIDATION_CODE_ERROR = 601;

    /**
     * FAIL_CODE_HAS_EXISTED_IDCARD
     */
    public static final int FAIL_CODE_HAS_EXISTED_IDCARD = 602;

    /**
     * FAIL_CODE_HAS_EXISTED_MOBILE
     */
    public static final int FAIL_CODE_HAS_EXISTED_MOBILE = 603;

    /**
     * FAIL_CODE_HAS_EXISTED_EMAIL
     */
    public static final int FAIL_CODE_HAS_EXISTED_EMAIL = 604;

    /**
     * FAIL_CODE_HAS_EXISTED_USERNAME
     */
    public static final int FAIL_CODE_HAS_EXISTED_USERNAME = 605;

    /**
     * FAIL_CODE_PASSWORD_NOT_STRONG
     */
    public static final int FAIL_CODE_PASSWORD_NOT_STRONG = 606;

    /**
     * FAIL_CODE_PASSWORD_IS_NULL
     */
    public static final int FAIL_CODE_PASSWORD_IS_NULL = 607;

    /**
     * FAIL_CODE_CREATED_ERROR
     */
    public static final int FAIL_CODE_CREATED_ERROR = 608;

    /**
     * FAIL_CODE_NO_VALIDATION_QUESTION_FOUND
     */
    public static final int FAIL_CODE_NO_VALIDATION_QUESTION_FOUND = 609;

    /**
     * FAIL_CODE_INCORRECT_VALIDATION_ANSWER
     */
    public static final int FAIL_CODE_INCORRECT_VALIDATION_ANSWER = 610;

    /**
     * FAIL_CODE_UPDATED_ERROR
     */
    public static final int FAIL_CODE_UPDATED_ERROR = 611;

    /**
     * FAIL_CODE_NO_PORTRAIT_FOUND
     */
    public static final int FAIL_CODE_NO_PORTRAIT_FOUND = 612;

    /**
     * FAIL_CODE_PASSWORD_MODIFY_ERROR
     */
    public static final int FAIL_CODE_PASSWORD_MODIFY_ERROR = 613;

    /**
     * FAIL_CODE_OLD_PASSWORD_MISMATCH
     */
    public static final int FAIL_CODE_OLD_PASSWORD_MISMATCH = 614;

    /**
     * FAIL_CODE_NEW_OLD_PASSWORD_SAME
     */
    public static final int FAIL_CODE_NEW_OLD_PASSWORD_SAME = 615;

    /**
     * FAIL_CODE_PASSWORD_ERROR
     */
    public static final int FAIL_CODE_PASSWORD_ERROR = 616;

    /**
     * FAIL_CODE_NO_RSA_PUBLICKEY_FOUND
     */
    public static final int FAIL_CODE_NO_RSA_PUBLICKEY_FOUND = 617;

    private ResultCode() {
        //do nothing
    }
}
