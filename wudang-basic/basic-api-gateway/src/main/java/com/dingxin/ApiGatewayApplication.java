package com.dingxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author : chuncheng.peng
 * @date : 2018-03-21
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
@EnableOAuth2Sso
public class ApiGatewayApplication {

    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
}
