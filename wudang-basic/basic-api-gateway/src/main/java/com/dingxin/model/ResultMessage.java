package com.dingxin.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : chuncheng.peng
 * @date : 2018-03-21
 */
public class ResultMessage {

    /**
     * RESULTMSGBOX
     */
    public static Map<Integer, String> RESULTMSGBOX = new HashMap<Integer, String>();

    private static final String SUCCESSMSG_ACTION_SUCCESS = "Completed successfully";

    private static final String FAILMSG_NO_APPLICANT_FOUND = "Not Found Applicant";

    private static final String FAILMSG_VALIDATION_CODE_ERROR = "Verify Validation Code error";

    private static final String FAILMSG_HAS_EXISTED_IDCARD = "Has existed Id Card";

    private static final String FAILMSG_HAS_EXISTED_MOBILE = "Has existed Mobile";

    private static final String FAILMSG_HAS_EXISTED_EMAIL = "Has existed Email";

    private static final String FAILMSG_HAS_EXISTED_USERNAME = "Has existed Username";

    private static final String FAILMSG_PASSWORD_NOT_STRONG = "Password is not strong enough";

    private static final String FAILMSG_PASSWORD_IS_NULL = "Password encrypted error";

    private static final String FAILMSG_CREATED_ERROR = "Record created error";

    private static final String FAILMSG_NO_VALIDATION_QUESTION_FOUND = "No Validation Question found";

    private static final String FAILMSG_INCORRECT_VALIDATION_ANSWER = "Incorrect Validation Answer provided";

    private static final String FAILMSG_UPDATED_ERROR = "Record updated error";

    private static final String FILMSG_NO_PORTRAIT_FOUND = "Not Found Portrait";

    private static final String FILMSG_PASSWORD_MODIFY_ERROR = "Password modified error";

    private static final String FILMSF_OLD_PASSWORD_MISMATCH = "Old password is not match";

    private static final String FILMSF_NEW_OLD_PASSWORD_SAME = "New and Old password is same";

    private static final String FILMSF_PASSWORD_ERROR = "Verify Password error";

    static {
        RESULTMSGBOX.put(ResultCode.SUCCESS, SUCCESSMSG_ACTION_SUCCESS);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_NO_APPLICANT_FOUND, FAILMSG_NO_APPLICANT_FOUND);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_VALIDATION_CODE_ERROR, FAILMSG_VALIDATION_CODE_ERROR);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_HAS_EXISTED_IDCARD, FAILMSG_HAS_EXISTED_IDCARD);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_HAS_EXISTED_MOBILE, FAILMSG_HAS_EXISTED_MOBILE);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_HAS_EXISTED_EMAIL, FAILMSG_HAS_EXISTED_EMAIL);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_HAS_EXISTED_USERNAME, FAILMSG_HAS_EXISTED_USERNAME);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_PASSWORD_NOT_STRONG, FAILMSG_PASSWORD_NOT_STRONG);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_PASSWORD_IS_NULL, FAILMSG_PASSWORD_IS_NULL);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_CREATED_ERROR, FAILMSG_CREATED_ERROR);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_NO_VALIDATION_QUESTION_FOUND, FAILMSG_NO_VALIDATION_QUESTION_FOUND);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_INCORRECT_VALIDATION_ANSWER, FAILMSG_INCORRECT_VALIDATION_ANSWER);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_UPDATED_ERROR, FAILMSG_UPDATED_ERROR);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_NO_PORTRAIT_FOUND, FILMSG_NO_PORTRAIT_FOUND);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_PASSWORD_MODIFY_ERROR, FILMSG_PASSWORD_MODIFY_ERROR);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_OLD_PASSWORD_MISMATCH, FILMSF_OLD_PASSWORD_MISMATCH);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_NEW_OLD_PASSWORD_SAME, FILMSF_NEW_OLD_PASSWORD_SAME);
        RESULTMSGBOX.put(ResultCode.FAIL_CODE_PASSWORD_ERROR, FILMSF_PASSWORD_ERROR);
    }

}
