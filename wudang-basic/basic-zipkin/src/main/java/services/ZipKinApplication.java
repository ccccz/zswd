package services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import zipkin.server.EnableZipkinServer;

/**
 * @author : chuncheng.peng
 * @date : 2018-05-21
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZipkinServer
public class ZipKinApplication {
	/**
	 * Main.
	 *
	 * @param args the args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ZipKinApplication.class, args);
	}
}
