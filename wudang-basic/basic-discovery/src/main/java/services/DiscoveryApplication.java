package services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author : chuncheng.peng
 * @date : 2018-05-21
 */
@SpringBootApplication
@EnableEurekaServer
public class DiscoveryApplication {
	/**
	 * Main.
	 *
	 * @param args the args
	 */
	public static void main(String[] args) {
		SpringApplication.run(DiscoveryApplication.class, args);
	}
}
